#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <math.h>   
#include <err.h>
#include <stdio.h>

#include "mars_simplex.hpp"
#include "mars_lagrange_element.hpp"
#include "mars_mesh.hpp"
#include "mars_bisection.hpp"
#include "mars_vtk_writer.hpp"
#include "mars_quality.hpp"
#include "mars_utils.hpp"
#include "mars_mesh_partition.hpp"
#include "mars_partitioned_bisection.hpp"
#include "mars_benchmark.hpp"
#include "mars_lepp_benchmark.hpp"
#include "mars_mesh_generation.hpp"
#include "mars_test.hpp"
#include "mars_ranked_edge.hpp"
#include "mars_oldest_edge.hpp"
#include "mars_longest_edge.hpp"
#include "mars_memory.hpp"
#include "mars_mesh_reader.hpp"
#include "mars_mesh_writer.hpp"
#include "mars_matrix.hpp"
#include "st_element.hpp"
#include "st_quadrature.hpp"
#include "matrix_free_operator.hpp"
#include "implicit_euler.hpp"
#include "precon_conjugate_grad.hpp"
#include "sparse_matrix.hpp"
#include "st_utils.hpp"
#include "st_rec_grad.hpp"
#include "st_amr.hpp"
#include "st_res_based.hpp"
#include "Kokkos_Core.hpp"
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv.hpp"
#include "KokkosBlas1_scal.hpp"
#include "mars_mesh_generation.hpp"
#include "vtu_writer.hpp"



namespace mars {
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    using KokkosVector = Kokkos::View<Real*>;

    ////////////////////////////////////////  test_space_time /////////////////////////////////////////
    // template<Integer Dim> 
    inline Real rhs_heat_ST(const Vector<Real,2> &x){ 
        // return  M_PI*std::cos(M_PI*x[1])*std::sin(M_PI*x[0]) - (-M_PI*M_PI*std::sin(M_PI*x[1])*std::sin(M_PI*x[0]));

        // return  M_PI  * 3.0/4.0 * std::sin(3.  * x[1]) * (4 * std::sin(3. * M_PI*x[0]/2.) + 3. *  M_PI * x[0] * std::cos(3. * M_PI*x[0]/2.)) + \
        // 3. * x[0] * std::cos(3. * M_PI * x[0]/2.) *  std::cos(3. * x[1]);

        return (-std::exp(-100.0*(x[1] - x[0]) *(x[1] - x[0]))*(- x[0] * x[0] + x[0])*(2.0*x[1] - 1) - std::exp(-100*(x[1] - x[0]) * (x[1] - x[0]))*(- x[1] * x[1] + x[1])*(200*x[1] - 200*x[0])*(- x[0] * x[0] + x[0])) -
        (std::exp(-100.0*(x[1] - x[0]) * (x[1] - x[0]))*(- x[1] * x[1] + x[1])*(200.0*x[1] - 200.0*x[0]) * (200.0*x[1] - 200.0*x[0])*(- x[0] * x[0] + x[0]) - 200.0*std::exp(-100.0*(x[1] - x[0]) *(x[1] - x[0]) )*(- x[1] * x[1] + x[1])*(- x[0] * x[0] + x[0]) - 2.0*std::exp(-100.0*(x[1] - x[0]) * (x[1] - x[0]))*(- x[1]*  x[1] + x[1])*(200.0*x[1] - 200.0*x[0])*(2.0*x[0] - 1) - 2.0*std::exp(-100*(x[1] - x[0]) * (x[1] - x[0]))*(- x[1] * x[1] + x[1]));  

        // return (std::sin(M_PI*x[0])*(2.0*x[1] - 2.0)) - (-M_PI*M_PI*std::sin(M_PI*x[0])*(x[1] - 1.) * (x[1] - 1.));

        // return (x[1]*x[0]*x[0]*(x[0] - 1.0) + x[0]*x[0]*(x[1] - 1)*(x[0] - 1.0)) - (4.0*x[1]*x[0]*(x[1] - 1.0) + 2.0*x[1]*(x[1] - 1)*(x[0] - 1.0));

        // return (-80*M_PI*std::exp(-200*(x[0] - std::cos((2*M_PI*x[1])/5)/2) * (x[0] - std::cos((2*M_PI*x[1])/5)/2))*std::sin((2*M_PI*x[1])/5)*(x[0] - std::cos((2*M_PI*x[1])/5)/2)) -
        // (std::exp(-200*(x[0] - std::cos((2*M_PI*x[1])/5)/2)*(x[0] - std::cos((2*M_PI*x[1])/5)/2))*(400*x[0] - 200*std::cos((2*M_PI*x[1])/5)) * (400*x[0] - 200*std::cos((2*M_PI*x[1])/5)) - 400*std::exp(-200*(x[0] - std::cos((2*M_PI*x[1])/5)/2) * (x[0] - std::cos((2*M_PI*x[1])/5)/2)) ) ;



 

    }

    // template<Integer Dim>
    inline Real exact_heat_ST(const Vector<Real, 2> &x){ 
        // return  std::sin(M_PI * x[0]) * std::sin(M_PI*x[1]);
        // return x[0] * std::cos(3. * M_PI * x[0]/2.) * std::sin(3. *x[1]);
        return (x[0] * x[0]  - x[0]) * (x[1] * x[1] - x[1]) * std::exp(-100 * ((x[0]-x[1]) * (x[0]-x[1])));

        // return std::sin(M_PI * x[0]) * (1.0-x[1]) *  (1.0-x[1]);

        // return (1-x[0]) * x[0] * x[0] * (1-x[1]) * x[1];

        // Real A = 1; 
        // Real sigma = 5e-02;
        // Real r = 0.5; 
        // Real p = 5;

        // Real gamma = r * std::cos(2.0 * M_PI * x[1] / p); 
        // return A * std::exp( - (( x[0]  - gamma) * ( x[0]  - gamma)) / (2*sigma*sigma));
    }

    inline Real init_heat_ST(const Vector<Real, 2> &x){ 
        // return std::sin(M_PI * x[0]) * std::sin(M_PI*0.0);

        // return std::sin(M_PI * x[0]) * (1.0-0.0) *  (1.0-0.0);

        return (x[0] * x[0]  - x[0]) * (0.0 * 0.0 - 0.0) * std::exp(-100 * ((x[0]-0.0) * (x[0]-0.0)));

        // return (1-x[0]) * x[0] * x[0] * (1-0.0) * 0.0;

 
        // Real A = 1; 
        // Real sigma = 5e-02;
        // Real r = 0.5; 
        // Real p = 5;

        // Real gamma = r * std::cos(2.0 * M_PI * 0.0 / p); 
        // return A * std::exp( - (( x[0]  - gamma) * ( x[0]  - gamma)) / (2*sigma*sigma));


    }

    inline Real rhs_heat(Real dt, const Vector<Real, 2> &x){ 
        return  (2*M_PI*M_PI-1) * std::exp(-dt) * std::sin(M_PI * x[0]) * std::sin(M_PI * x[1]);
    }

    inline Real exact_heat(Real dt, const Vector<Real, 2> &x){ 
        return  std::exp(-dt) * std::sin(M_PI * x[0]) * std::sin(M_PI * x[1]);
    
    } 

}


void test_space_time(int &n_el){

using namespace mars;
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    
    VTUMeshWriter<Mesh2> w;
    CSVWriter csv;
    CSVReader csv_read;
    Mesh2 mesh;
    std::cout << n_el << std::endl;
    generate_square(mesh,20,20);

    // read("/Users/liudmilakaragyaur/code/test2.msh", mesh, true);
    // mesh.describe(std::cout);
    // mark_boundary(mesh);   
    // mesh.build_dual_graph(); 

    // for (int j =0; j < mesh.n_nodes(); j++){
    //     mesh.point(j)[0] = 1 - mesh.point(j)[0];
    // }

    Bisection<Mesh2> b(mesh);
    b.uniform_refine(n_el); b.clear();
    mesh.clean_up();
    mesh.update_dual_graph();

    Integer n_el_old = mesh.n_elements();
    Integer n_nodes_old = mesh.n_nodes();
    std::cout<< "Number of elements: " << mesh.n_active_elements() <<std::endl;

    write("/Users/liudmilakaragyaur/code/space_time_error_estimator/mesh.msh", mesh);

    bool space_time = true;
    MassMatrixAssembler<2,2> M;
    StiffnessMatrixAssembler<2,2> S;

    SparseMatrix mass_no_upwind = assemble_mass_stiffness(mesh, M, S, space_time, true, false);
    SparseMatrix stiffness_no_upwind = assemble_mass_stiffness(mesh,M, S, space_time, false, false);


    std::vector<bool> node_is_boundary(mesh.n_nodes(), false);
    Integer side_tag = 4; // or 4
    boundary_nodes(mesh, node_is_boundary, space_time, side_tag); 

    std::string path3 = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/result_st.vtu";
    std::string path_exact = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/result_exact.vtu";
    std::string path_res_bas = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/error_res_based.vtu";
    std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/error_grad_rec.vtu";
    std::string path_unif_err = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/uniform_ref";

    
    // for (auto i: node_is_boundary)
    //     std::cout << i << ' ';

    bool CG = true; // CG space-time Heat Equation
    bool IE = false; //IE space + time Heat Equation
    const size_t N = mesh.n_nodes();

    if (CG){
        Kokkos::View<Real*> st_rhs("st_rhs", N);
        Kokkos::View<Real*> init_guess("init_guess", N);
        fill_vector(init_guess, N, 0.5);
        Kokkos::View<Real*> result_st("result_st", N);
        Kokkos::View<Real*> sol_exact("sol_exact", N);
        Kokkos::View<Real*> energy_norm("energy_norm", N);
        for (int k = 0; k<N; ++k){
            sol_exact(k) = exact_heat_ST(mesh.point(k));
        }   

        for (int k = 0; k<N; ++k){
            // if(mesh.point(k)[1] == 1){
            //     st_rhs(k) = 0.0;
            // } else  {
                st_rhs(k) = rhs_heat_ST(mesh.point(k));
            // }
            
        }  
        // display_vector(st_rhs, N);

#if 1//  upwind  version
        SparseMatrix mass_upwind =  assemble_mass_stiffness(mesh, M, S, space_time, true, true);
        SparseMatrix stiffness_upwind =  assemble_mass_stiffness(mesh,M, S, space_time, false, true);


        Kokkos::View<Real*> Mb("Mb", N );
        KokkosSparse::spmv("N", 1.0, mass_upwind, st_rhs, 0.0, Mb);

        assign_boundary_cond(mesh, stiffness_upwind, Mb, node_is_boundary); 
        assign_init_cond(mesh, init_heat_ST, Mb);

        // std::string path_m_rhs0 = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/uniform_ref/2D/example_3/matrix_rhs"; 
        // csv.write_sparse(path_m_rhs0 + "/matrix" + std::to_string(n_el) + ".csv", N, stiffness_upwind);
        // csv.write_sparse_vector(path_m_rhs0 + "/rhs" + std::to_string(n_el) + ".csv", N, Mb);


        Real num_iter;
        preconCG(stiffness_upwind, mass_upwind, Mb, node_is_boundary, 0, init_guess, result_st, num_iter); 

        // display_vector(result_st, N);

        w.write_vtu(path3, mesh, result_st);
        w.write_vtu(path_exact, mesh, sol_exact);

        // csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/results/csv/error/sol_approx.csv",N, result_st);
        // csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/results/csv/error/sol_exact.csv",N, sol_exact);


#else // no upwind version
        std::cout << "  NO UPWIND!!!!!!!!!!!!!!!!  "  <<  std::endl;
        Kokkos::View<Real*> Mb("Mb", N );
        KokkosSparse::spmv("N", 1.0, mass_no_upwind, st_rhs, 0.0, Mb);


        assign_boundary_cond(mesh, stiffness_no_upwind, Mb, node_is_boundary); 
        assign_init_cond(mesh, init_heat_ST, Mb);
        std::string path_m_rhs0 = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/uniform_ref/2D/example_3/matrix_rhs"; 
        csv.write_sparse(path_m_rhs0 + "/matrix" + std::to_string(n_el) + ".csv", N, stiffness_no_upwind);
        csv.write_sparse_vector(path_m_rhs0+ "/rhs" + std::to_string(n_el) + ".csv", N, Mb);


        Real num_iter;
        preconCG(stiffness_no_upwind, mass_no_upwind, Mb, node_is_boundary, 0, init_guess, result_st, num_iter); 

        // display_vector(result_st, N);

        w.write_vtu(path3, mesh, result_st);
        w.write_vtu(path_exact, mesh, sol_exact);

        // csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/results/csv/error/sol_approx.csv",N, result_st);
        // csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/results/csv/error/sol_exact.csv",N, sol_exact);

#endif
        
        //compute energy norm 

        std::vector<Integer> elements_to_refine_RB;
        Real TOL  = 1e-6;
        Real mean_error_RB;
        res_based(mesh, result_st, rhs_heat_ST, elements_to_refine_RB,mean_error_RB, -1,-1, TOL, path_res_bas);
        std::cout<< "Original error RB: " << mean_error_RB <<std::endl;


        std::vector<Integer> elements_to_refine;
        Real mean_error_RG;
        rec_grad(mesh, mass_no_upwind, result_st, node_is_boundary, TOL, elements_to_refine,mean_error_RG, path_error);
        std::cout<< "Original error RG: " << mean_error_RG <<std::endl;
        Real TOL1 = 1e-6;
        Real TOL2 = 1e-7;
        Integer max_it = 100;
        
#if 1// adaptive
        // adaptive_scheme(mesh, TOL1, TOL2, max_it, result_st, elements_to_refine, side_tag, rhs_heat_ST, init_heat_ST, exact_heat_ST, true);
        adaptive_scheme( mesh, TOL1, TOL2, max_it, result_st, elements_to_refine_RB, side_tag, rhs_heat_ST, init_heat_ST,exact_heat_ST, true, false);
#else  // uniform
        Real tot_error = 0;
        compute_exact_error(mesh, sol_exact, result_st,tot_error);
        std::cout<< "True error: " << tot_error << std::endl;

        FILE* file = fopen("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/uniform_ref/output.csv", "a");
        fprintf(file, "%ld, %e, %e, %e\n",n_nodes_old, mean_error_RB, mean_error_RG,tot_error);
        fclose (file);
#endif
        // std::vector<Real> read_test;
        // csv_read.read_csv("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/error/error_2D.csv", read_test);

        //////////////////////////////////////////////

        // std::string path_sol = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/uniform_ref/2D/example_3/matrix_rhs"; 

        // std::vector<Real> read_sol;
        // csv_read.read_csv(path_sol + "/sol" + std::to_string(n_el) + ".csv", read_sol);  //
        // // csv_read.read_csv(path_sol + "/sol11.csv", read_sol);  //
        

        // const size_t N_read = read_sol.size();
        // Kokkos::View<Real*> direct_sol("direct_sol", N_read);

        // vec_to_kokkos(read_sol,direct_sol);
        // std::cout << direct_sol.extent(0) << std::endl;

        // w.write_vtu("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/sol" + std::to_string(n_el) + ".vtu", mesh, direct_sol);
        // w.write_vtu("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/sol11.vtu", mesh, direct_sol);



        // Real tot_error = 0;
        // compute_exact_error(mesh, sol_exact, direct_sol, tot_error);
        // std::cout<< "True error: " << tot_error <<std::endl;

        // FILE* file = fopen("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/uniform_ref/output_direct.csv", "a");
        // fprintf(file, "%ld, %f\n",n_nodes_old,tot_error);
        // fclose (file);


        //////////////////////////////////////////////



        
    } else if (IE) {

        Real t = 0.0;
        Real T = 2.0;
        const size_t ts =15;

        std::vector<Real> time = linspace(t, T, ts);
        Kokkos::View<Real*> res("res", N );
        Kokkos::View<Real*> x_0("x_0", N );
        Kokkos::View<Real*> rhs("x_0", N ); // this stays empty 

        // Initial solution
        for (int i=0; i<N;i++){
            x_0(i)  = 1.0;
            // exact_heat(0.0, mesh.point(i));
        } 

        Real ciao = mesh.point(0)[0];
        Integer idx = 0;
        assign_boundary_cond(mesh, stiffness_no_upwind, rhs, node_is_boundary); 

        implicit_euler(mesh, stiffness_no_upwind, mass_no_upwind, x_0, res, time, node_is_boundary,rhs_heat);

        Real dt = time[2] - time[1];
        Kokkos::View<Real*> exact("exact", N );
        Kokkos::View<Real*> exact_1("exact", N );
        for (int k=0;  k< N; k++){
            assert(k < mesh.n_nodes());
            exact(k) = exact_heat(time[time.size()-1], mesh.point(k));
        }

        std::string path = "/Users/liudmilakaragyaur/code/space_time_error_estimator/results/result_SP.vtu";
        std::string path1 = "/Users/liudmilakaragyaur/code/space_time_error_estimator/results/result_exact.vtu";
        w.write_vtu(path, mesh, res);
        w.write_vtu(path1, mesh, exact);




    }
}




int main(int argc, char *argv[])
{
	using namespace mars;

    Kokkos::initialize(argc,argv);

    char *end_ptr = argv[1];
    int level = strtol(argv[1], &end_ptr, 10);

    test_space_time(level);

    Kokkos::finalize();

    return 0; 
}
