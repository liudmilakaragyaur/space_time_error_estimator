#ifndef ST_CSV_WRITER
#define ST_CSV_WRITER

#include <iostream>
#include <fstream>
#include <string>

#include "mars_base.hpp"
#include "mars_mesh.hpp"

namespace mars {
    class CSVWriter {
    public: 
        using KokkosVector = Kokkos::View<Real*>;
        using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
        
        template<typename T>
        bool write_value(const std::string &path, const std::vector<T> &values) {

            std::ofstream os;
            os.open(path.c_str());
            if (!os.good()) {
                os.close();
                return false;
            }

            int  N = values.size();
            // std::cout << "SIZE -> " << N << std::endl;
            for (int i =0; i < N; i++){
                if (i == N-1){
                    os << values[i];
                } else {
                    os << values[i] << ",";
                }
                
            }
            
            os.close();
            return true;
        }


        bool write_sparse(const std::string &path, const Integer &N, const SparseMatrix &mat) {

            std::ofstream os;
            os.open(path.c_str());
            if (!os.good()) {
                os.close();
                return false;
            }

            for(Integer i = 0; i < N; ++i) {
            auto row = mat.row(i);
            auto n_vals = row.length; // is the number of entries in the row 
            //        assert(n_vals != 0);
            for(Integer k = 0; k < n_vals; ++k) {
                auto value = row.value(k);
                auto j = row.colidx(k); // returns a const reference to the column index of the k-th entry in the row </li>
                os << i << "," << j << "," << value << "\n";
            }
        }
            
            os.close();
            return true;
        } 


        bool write_sparse_vector(const std::string &path, const Integer &N, const KokkosVector &values) {

            std::ofstream os;
            os.open(path.c_str());
            if (!os.good()) {
                os.close();
                return false;
            }

            for (int i =0; i < N; i++){
                if (i == N-1){
                    os << values(i);
                } else {
                    os << values(i) << ",";
                }
                
            }
            
            os.close();
            return true;
        }

    }; 

    class CSVReader {
    public: 
        using KokkosVector = Kokkos::View<Real*>;
        using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
        
        template<typename T>
        bool read_csv(const std::string &path, std::vector<T> &vec) {

            std::ifstream is(path);
            if (!is.good())
            {
                return false;
            }

            Real value = -1;
            std::string line;
            while (is.good())
            {
                std::getline(is, line);
                value = atof(line.c_str());
                vec.push_back(value);
            }


        }
    };


}

#endif //ST_CSV_WRITER