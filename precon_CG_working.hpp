#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>

#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include <err.h>
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv_spec.hpp"
#include <Kokkos_Core.hpp>
#include <KokkosSparse_spmv.hpp>
#include<KokkosBlas1_axpby.hpp>
#include<KokkosBlas1_mult.hpp>
#include<KokkosBlas1_dot.hpp>
#include<KokkosBlas1_scal.hpp>
#include<KokkosBlas1_nrm2.hpp>

//Note on Blas1 Mult. We want to do z[i] = b*z[i] + a*x[i]*y[i] --> use as KokkosBlas::mult(b, z, a, x, y); 
namespace mars{
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    using KokkosVector = Kokkos::View<Real*>;

    void preconCG(const SparseMatrix &A, const KokkosVector &b, std::vector<bool> &node_is_boundary, KokkosVector &result) {
        Real TOL =  1.0e-8; //set the tolerance
        Integer max_iter = 500;
        Integer N =  A.numRows();

        // Kokkos::View<Real*> x_0("X", N ); // Real * idicates that N  by 1 array of type int 
        Kokkos::View<Real*> x_0("Z", N );
        Kokkos::View<Real*> x_1("Z", N );
        Kokkos::View<Real*> r_0("Z", N );
        Kokkos::View<Real*> r_1("Z", N );
        Kokkos::View<Real*> z_0("Z", N );
        Kokkos::View<Real*> p_0("Z", N );
        Kokkos::View<Real*> z_1("Z", N );
        Kokkos::View<Real*> p_1("Z", N );
        Kokkos::View<Real*> Ap("Z", N );
    
        

        for(int i=0;i<N;i++){
            if (node_is_boundary[i]  == 1){
                x_0(i)=b(i); 
            } else {
                x_0(i)=1.0;  
            }
        }

        // std::cout << "NEW" << ' ';
        // for(int i=0;i<N;i++){
        //     std::cout<<x_0(i)<<std::endl;
        // }
     
        KokkosSparse::spmv("N", 1.0, A, x_0, 0.0, r_0); // A*x_0

        KokkosBlas::axpy(-1.0, r_0, b); // b - A*x_0
        Kokkos::deep_copy (r_0, b);

        //Preconditioner 
        Kokkos::View<Real*> M("M", N ); 

        for(Integer i = 0; i < N; ++i) {
            auto row = A.row(i);
            auto n_vals = row.length;
            for(Integer k = 0; k < n_vals; ++k) {
                auto j = row.colidx(k);
                if (i==j){
                    M(i) = 1.0/row.value(k);
                }
            }
        }

        KokkosBlas::mult(0.0, z_0, 1.0, r_0, M);

    
 
        int count = 0;
        // Real eps = KokkosBlas::dot(r_0,r_0);
        Kokkos::deep_copy (p_0, z_0);
        double eps = KokkosBlas::nrm2(r_1);
        while (count < max_iter){

            count++;
            Real alpha_num = KokkosBlas::dot(r_0,z_0); // (r_k)^T * z_k
            KokkosSparse::spmv("N", 1.0, A, p_0, 0.0, Ap); // A * p_k
            
            Real alpha_den = KokkosBlas::dot(p_0,Ap); // (p_k)^T * A * p_k
            if (alpha_den == 0){
                // terminate the loop
                break;
            }
            Real alpha_new =  alpha_num/alpha_den; // alpha 

            KokkosBlas::scal(x_1,alpha_new,p_0);// alpha_k *  p_k

            KokkosBlas::axpy(1.0, x_0, x_1); // x_new = x_old + alpha_k *  p_k
            // Kokkos::deep_copy (x_1, x_0);

            KokkosBlas::scal(r_1,alpha_new,Ap);// alpha_new * A * p_k

            // KokkosBlas::axpy(-1.0, r_0, r_1); // r_new = r_old - alpha_new * A * p_k
            KokkosBlas::axpby(1.0, r_0, -1.0, r_1);
            // Kokkos::deep_copy (r_1, r_0);
            KokkosBlas::mult(0.0, z_1, 1.0, r_1, M); // z_new = preconditioner * r_new 

    

            Real beta_num = KokkosBlas::dot(z_1,r_1); // (z_new)^T * r_new 
            
            Real beta_den = KokkosBlas::dot(z_0, r_0); // (z_old)^T * r_old
            if (beta_den == 0){
                // terminate the loop
                break;
            }
            
            Real beta_new = beta_num/beta_den; // beta_new 
            KokkosBlas::scal(p_1,beta_new,p_0); // beta_new * p_old 
            KokkosBlas::axpy(1.0, z_1, p_1); // p_new = z_new + beta_new * p_old 

      

            Kokkos::deep_copy (x_0, x_1);
            Kokkos::deep_copy (p_0, p_1);
            Kokkos::deep_copy (r_0, r_1);
            Kokkos::deep_copy (z_0, z_1);

            double eps = KokkosBlas::nrm2(r_0);
            if (eps < TOL){
                // terminate the loop
                break;
            }
        }

        Kokkos::deep_copy (result, x_0);

    }

}