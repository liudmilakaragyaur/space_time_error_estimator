#ifndef ST_RESBASED_HPP
#define ST_RESBASED_HPP

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <cstdlib>	
#include <cmath> 

#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include <err.h>
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv_spec.hpp"
#include <Kokkos_Core.hpp>
#include <KokkosSparse_spmv.hpp>
#include<KokkosBlas1_axpby.hpp>
#include<KokkosBlas1_mult.hpp>
#include<KokkosBlas1_dot.hpp>
#include<KokkosBlas1_scal.hpp>
#include<KokkosBlas1_nrm1.hpp>
#include<KokkosBlas1_nrm2.hpp>
#include<KokkosSparse_spadd.hpp>

namespace mars{

    using KokkosVector = Kokkos::View<Real*>;
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;

    template<Integer Dim, Integer ManifoldDim>
    void integration_edge(
    FE<Dim,ManifoldDim> &element,
    Real jump, 
    const std::vector<Vector<Real, ManifoldDim-1>> &qp, 
    const std::vector<Real> &qw,
    Real determinant,
    Real measure,
    Real &result) 
    {
        
        //iterate over lenght of points or weights 
        for (int i = 0; i<qp.size(); i++){
            result += determinant *  qw[i] * measure * jump * jump;
        }


    }


    template<Integer Dim, Integer ManifoldDim, class F>
    void integration_element(
    FE<Dim,ManifoldDim> &element,
    F rhs_func, 
    Vector<Real, ManifoldDim+1> &y,  
    const std::vector<Vector<Real, ManifoldDim>> &qp, 
    const std::vector<Real> &qw,
    Real &result) 
    {
        for (int j = 0; j <  qw.size(); j++){
            element.init_fun(qp[j]);
            element.init_grad();
            Real q_new = qw[j] * element.ref_measure;

            Real f = 0.0;
            f = rhs_func(qp[j]);

            Real int_res = 0.0;
            for (int i = 0; i<ManifoldDim+1; i++){
                int_res += y(i) * element.grad[i][ManifoldDim-1]; 
            }

            Real diff = 0.0;
            diff = f - int_res;

            result += element.det_J * q_new * diff * diff;
            
            // std::cout <<  element.det_J << " | " << q_new << " | " << diff * diff <<  "RESULT -> " << result << std::endl;

        }
        
       
    }

    ////////////////////////////////////////////////////////////////////

    template<Integer Dim, Integer ManifoldDim, class F>
    void res_based(
    Mesh<Dim, ManifoldDim> &mesh, 
    KokkosVector &approx_sol,
    F rhs_func,
    std::vector<Integer> &elements_to_refine,
    Real &mean_error,
    Integer init_cond_tag,
    Integer bound_cond_tag,
    Real TOL,
    std::string path
    ) 
    {
        const size_t N = mesh.n_nodes();
        const size_t N_el = mesh.n_active_elements();

        CSVWriter csv;

        // Quadrature<1,Dim> q_el; // 2 +  3 D
        Quadrature<5,Dim> q_el; //2D
        // Quadrature<27,Dim> q_el; //2D

        // Quadrature<256,Dim> q_el;  // 4D

        Quadrature<4,Dim-1> q_ed; 

        std::vector<Vector<Real, ManifoldDim>> q_el_p;
        std::vector<Vector<Real, ManifoldDim-1>> q_ed_p;
        std::vector<Real> q_el_w, q_ed_w;



        q_el_p = q_el.get_points();
        q_el_w = q_el.get_weights(); 

        q_ed_p = q_ed.get_points();
        q_ed_w = q_ed.get_weights();

        KokkosVector E("E", N_el);
        KokkosVector Err("Err", N_el);

        FE<Dim,ManifoldDim> element;
        FE<Dim,ManifoldDim-1> edge;
        Simplex<Dim, ManifoldDim-1> side_el;

        KokkosVector tot_area("tot_area", N_el);

        mesh.build_dual_graph(); 

        // mesh.describe_dual_graph(std::cout);

        std::vector<Vector<Integer, ManifoldDim+1>> common_sides;
        std::vector<Vector<Real, ManifoldDim+1>> otwrd_normals;

        for (int k = 0; k < N_el; ++k){
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(k);
            std::array<Integer, ManifoldDim+1> &nodes = mesh.elem(k).nodes;

            std::array<Integer,ManifoldDim+1> &adj = mesh.dual_graph().adj(k);

            Integer n_adj =  mesh.dual_graph().n_adjacients(k);

            Vector<Integer, ManifoldDim+1> common_sides_prov;
            Vector<Real, ManifoldDim+1> otwrd_normals_prov;
            common_sides_prov.set(-1.0);
            otwrd_normals_prov.set(-1.0);

            for(int a = 0; a < ManifoldDim+1; a++) {
                
                if (adj[a] != INVALID_INDEX){
                    const Simplex<Dim, ManifoldDim> &simplex_adj = mesh.elem(adj[a]);
                    std::array<Integer, ManifoldDim+1> &nodes_adj = mesh.elem(adj[a]).nodes;


                    element.init_jacobian(mesh, adj[a]); 

                    Integer c_side_1 =  mesh.common_side_num(k, adj[a]); // idx of  the side  of element k 
                    Integer c_side_2 =  mesh.common_side_num(adj[a],k); // idx of  the side  of the adj  element

                
                    common_sides_prov(a) = c_side_1;
                    simplex_adj.side(c_side_2, side_el);

                    // Compute gradient 
                    Vector<Real,ManifoldDim+1> coeff_adj; 
                    coeff_adj.zero();
                    for (int i = 0; i<ManifoldDim+1;  i++){
                        coeff_adj(i) = approx_sol(nodes_adj[i]);
                    }


                    
                    Vector<Real,ManifoldDim> real_grad_adj; 
                    real_grad_adj.zero();
                    element.sum(mesh, q_el_p, coeff_adj, real_grad_adj); 

                    // Compute normal 

                    Vector<Real, Dim> nrm_adj = normal(side_el, mesh.points());
                                
                    otwrd_normals_prov(a) = dot(nrm_adj, real_grad_adj);



                }

            }

            common_sides.push_back(common_sides_prov);
            otwrd_normals.push_back(otwrd_normals_prov);


           
        } 


        // std::cout << common_sides.size() << " | " << otwrd_normals.size() << " | " << N_el << std::endl;

        for (int k = 0; k < N_el; ++k){

            element.init_jacobian(mesh, k); 
            element.elem_diam(mesh, k);

            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(k);
            std::array<Integer, ManifoldDim+1> &nodes = mesh.elem(k).nodes;


            Integer N_sds = n_sides(simplex);

            Vector<Real,ManifoldDim+1> coeff; 
            coeff.zero();
            for (int i = 0; i<ManifoldDim+1;  i++){
                coeff(i) = approx_sol(nodes[i]);
            }

            Vector<Real,ManifoldDim> real_grad; 
            real_grad.zero();
            element.sum(mesh, q_el_p, coeff, real_grad); 
            
        
            Real interior_error = 0.0; 
            integration_element(element, rhs_func, coeff, q_el_p, q_el_w, interior_error);            

            // mesh.describe_element(k,std::cout);
            
            Vector<bool, ManifoldDim+1> edge_is_boundary; 
            edge_is_boundary.zero();
            boundary_sides(mesh, k, edge_is_boundary, init_cond_tag, bound_cond_tag);
            // std::cout << "Element: " << k << std::endl;
            // edge_is_boundary.describe(std::cout);

            Real element_area = 0.0;
            Real side_error = 0.0;
            for (int d = 0; d < N_sds; ++d){
                simplex.side(d,side_el);
                std::array<Integer, ManifoldDim> &nodes_edge = side_el.nodes;
                element.init_jacobian_edge(mesh,k,d);
                Real jump_el = 0.0;

                if (!edge_is_boundary[d]) {
                    Vector<Real, Dim> nrm = normal(side_el, mesh.points());
                    
                    jump_el += dot(nrm,real_grad);
                    for (int i = 0; i<ManifoldDim+1; i++){
                        if (common_sides[k][i] == d){
                            jump_el += otwrd_normals[k][i];
                        }
                    }

                } else if (simplex.side_tags[d] == bound_cond_tag) {
                    Vector<Real, Dim> nrm2 = normal(side_el, mesh.points());
                    jump_el -= dot(nrm2,real_grad);

                }

                Real intgr = 0.0;
                integration_edge(element, jump_el, q_ed_p, q_ed_w, element.det_J_edge, edge.ref_measure, intgr);
                side_error += element.diam * intgr;
                element_area +=  element.det_J_edge;
                
            }

            tot_area(k) = element_area;
            // std::cout << element.diam << " | " << side_error << " | " << interior_error << std::endl;
            E(k) = (element.diam * side_error + element.diam * element.diam * interior_error) ;  // // 

            Err(k) = std::sqrt(E(k));
            
            assert(!std::isnan(E(k)));
            assert(E(k) >= 0);
            assert(Err(k) >= 0);
            
            // std::cout << E(k)<< std::endl;
            // std::cout << "======================"<< std::endl;


            // if (E(k)/tot_area(k) > TOL){
            //     elements_to_refine.push_back(k);
            // }
            
        }


        Real nrm_inf = KokkosBlas::nrminf(Err);
        Real nrm_inf2 = KokkosBlas::nrminf(E);

        // mean_error =  nrm_inf; 
        // std::cout << "NORM INF: " << nrm_inf << std::endl;

        // display_vector(E,N_el);
        Real nrm_tot = std::sqrt(KokkosBlas::nrm1(E));
        // Real nrm_tot = KokkosBlas::nrm1(E);
        mean_error =  nrm_tot;
        // std::cout << "NORM TOT: " << nrm_tot << std::endl;



        for (int k = 0; k < N_el; k++){
            if (Err(k) >=  0.008*nrm_inf ){ //0.1*nrm_inf
                elements_to_refine.push_back(k);
            }  
        }
        
        
        if (Dim != 4){
            VTUMeshWriter<Mesh<Dim,ManifoldDim>> w;
            w.write_vtu(path, mesh, Err, true);
        }
        

    }

    





}


#endif //ST_RESBASED_HPP 