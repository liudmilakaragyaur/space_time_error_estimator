#ifndef ST_RECGRAD_HPP
#define ST_RECGRAD_HPP

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <cstdlib>	
#include <cmath> 

#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include "st_quadrature.hpp"
#include <err.h>
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv_spec.hpp"
#include <Kokkos_Core.hpp>
#include <KokkosSparse_spmv.hpp>
#include<KokkosBlas1_axpby.hpp>
#include<KokkosBlas1_mult.hpp>
#include<KokkosBlas1_dot.hpp>
#include<KokkosBlas1_scal.hpp>
#include<KokkosBlas1_nrm1.hpp>
#include<KokkosBlas1_nrm2.hpp>
#include<KokkosBlas1_nrminf.hpp>
#include<KokkosSparse_spadd.hpp>


namespace mars{

    using KokkosVector = Kokkos::View<Real*>;
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;


    template<Integer Dim, Integer ManifoldDim>
    void compute_error(
    FE<Dim, ManifoldDim> &element, 
    std::vector<Vector<Real, ManifoldDim>> &x,
    Vector<Real,ManifoldDim> &y, 
    const std::vector<Real> &qw, 
    const std::vector<Vector<Real, Dim>> &qp,
    Real &error){


        Vector<Real,ManifoldDim> g_r; 
        Vector<Real,ManifoldDim> diff; 

        for (int j = 0; j <  qw.size(); j++){
            element.init_fun(qp[j]); 
            const Real q_new = qw[j] * element.ref_measure;

            g_r.zero();
            for (int i = 0; i <  ManifoldDim+1; i++){
                g_r += element.fun[i] * x[i];
            }
            
            diff.zero();

            diff = y - g_r;
            Real scal = dot(diff,diff);
            error += element.det_J * q_new * scal;


        }
   

    }




    template<Integer Dim, Integer ManifoldDim>
    void rec_grad(Mesh<Dim, ManifoldDim> &mesh, 
    SparseMatrix &mass, 
    KokkosVector &result_rg,
    std::vector<bool> &node_is_boundary,
    Real TOL,
    std::vector<Integer> &elements_to_refine,
    Real &mean_error,
    std::string path
    )
    
    {

        const size_t N = mesh.n_nodes();
        const size_t ND = mesh.n_nodes() * Dim;
        const size_t N_el = mesh.n_elements();

        CSVWriter csv;

        Quadrature<1,Dim> q0; 
        Quadrature<0,Dim> q2; // 5 for 3D; 1 for 2D
        // Quadrature<5,Dim> q4;

        // if (Dim == 4){
            // Quadrature<256,Dim> q4;
            // Quadrature<27,Dim> q4;
            Quadrature<4,Dim> q4;
        // } else {
        // }
        // 

        std::vector<Vector<Real, Dim>> qps, qp, qp4;
        std::vector<Real> qws, qw, qw4;
       

        qps = q0.get_points();
        qws = q0.get_weights(); 

        qp = q2.get_points();
        qw = q2.get_weights();

        qp4 = q4.get_points();
        qw4 = q4.get_weights();


        KokkosVector V("V", ND);
        KokkosVector V_mass("V", ND);
        KokkosVector M("M", N); // inverse of the diagonal mass matrix
        KokkosVector lumped("lumped", N );
        KokkosVector E("E", N_el);
        KokkosVector Err("Err", N_el);


        lumped_mat(mass, lumped); 
        invert_diag_vec(lumped,M,N);


        std::vector<Vector<Real, ManifoldDim>> RG;
   
        FE<Dim,ManifoldDim> element;
        Simplex<Dim, ManifoldDim-1> side_i;
        for (int k = 0; k < mesh.n_elements(); k++) {

            element.init_jacobian(mesh, k); 
             
            
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(k);
            std::array<Integer, ManifoldDim+1> &nodes = mesh.elem(k).nodes;


            Vector<Real,ManifoldDim+1> coeff; 
            coeff.zero();

            for (int i = 0; i<ManifoldDim+1;  i++){
                coeff(i) = result_rg(nodes[i]);
            }

            Vector<Real,ManifoldDim> real_grad; 
            real_grad.zero();
            element.sum(mesh, qps, coeff, real_grad); // Real gradient qps
            RG.push_back(real_grad);

            std::array<Vector<Real,ManifoldDim+1>, ManifoldDim> result_grad;

            for (int i = 0; i<ManifoldDim;  i++){
                result_grad[i].zero();
                element.integrate(mesh, qp4, qw4, real_grad[i], result_grad[i]);
            }

                    

            for (int j = 0; j<ManifoldDim; j++){
                for (int i = 0; i<ManifoldDim+1;  i++){
                    V(nodes[i]*Dim + j)  +=  result_grad[j][i] * M(nodes[i]);
                    V_mass(nodes[i]*Dim + j)  +=  result_grad[j][i];

                }

            }

        }



        KokkosVector Ev("Ev", N);
        KokkosVector VX_res("vx", N);
        KokkosVector VY_res("vy", N);
        KokkosVector VZ_res("vz", N);
        bool is_lumped = true;
        if (!is_lumped){
            Kokkos::View<Real*> init_guess("init_guess", N );
            fill_vector(init_guess, N, 0.5);   
            Real num_iter; 
            KokkosVector sol("sol", N);

            for (int z = 0; z<ManifoldDim; z++){
                KokkosVector v_dim("v_dim", N);
                for (int  i = 0; i< mesh.n_nodes(); i++){
                    v_dim(i) = V_mass(i*Dim + z);
                }

                preconCG(mass, mass, v_dim, node_is_boundary, 0, init_guess, sol, num_iter); 
                if (z==0){
                    std::string path_matrix_mass = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/mass.csv";
                    csv.write_sparse(path_matrix_mass, N, mass);
                    csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/VX.csv", N, v_dim);
                    display_vector(sol,N);

                }


                if (z==0){
                    Kokkos::deep_copy (VX_res, sol);
                } else if (z==1){
                    Kokkos::deep_copy (VY_res, sol);
                } else if (z==2){
                    Kokkos::deep_copy (VZ_res, sol);
                }
                
            }

        }


        
        for (Integer k = 0; k < mesh.n_elements(); k++) {

            element.init_jacobian(mesh, k); 
            
            const Simplex<Dim,ManifoldDim> &simplex = mesh.elem(k);
            std::array<Integer, ManifoldDim+1> &nodes = mesh.elem(k).nodes;

            Vector<Real, ManifoldDim> true_grad = RG[k];
            std::array<Vector<Real, ManifoldDim+1>, ManifoldDim> V_loc;

            if(is_lumped){
                for (int j = 0; j<ManifoldDim; j++){
                    V_loc[j].zero();
                    for (int i = 0; i<ManifoldDim+1;  i++){
                        V_loc[j][i] = V(nodes[i]*Dim + j);

                    }

                }
            } else {
                for (int j = 0; j<ManifoldDim; j++){
                    V_loc[j].zero();
                    for (int i = 0; i < ManifoldDim+1; i++){
                        
                        if (j == 0){
                            V_loc[j][i] = VX_res(nodes[i]);
                        }  else if (j==1) {
                            V_loc[j][i] = VY_res(nodes[i]); 
                        } else{
                            V_loc[j][i] = VZ_res(nodes[i]); 
                        }

                        
                    }


                }
                
            }

            std::vector<Vector<Real, ManifoldDim>> rec_gradients; 

            for (int j = 0; j<ManifoldDim+1; j++){
                Vector<Real,ManifoldDim> grad1; 
                grad1.zero();
                for(int i = 0; i< ManifoldDim; i++){
                    grad1[i] = V_loc[i][j];
                    assert(!std::isnan(grad1[i]));
                    
                }
                
                rec_gradients.push_back(grad1);

            }
            


            Real err = 0.0;
            compute_error(element, rec_gradients, true_grad, qw4, qp4, err);
            assert(!std::isnan(err));
            assert(err >= 0);
            Real sum_w = 0.0;
            for (int i = 0; i < qws.size(); i++){
                sum_w  += qws[i] * element.ref_measure; 
            }

            E(k) = err; 

            Err(k) = std::sqrt(err);
            // assert(!std::isnan(E(k)));
            // assert(E(k) >= 0);
            
            // if (E(k)/(element.det_J *  sum_w)  > TOL){
            //     elements_to_refine.push_back(k);
            //     // E(k) = 1; 
            // } 
            

            
            // Vector<Real,ManifoldDim+1> e_plot; 
            // e_plot.zero();
            // element.integrate(mesh, qps, qws, E(k), e_plot);

            // for (int i = 0; i<ManifoldDim+1;  i++){
            //     Ev(nodes[i]) += e_plot(i) * M(nodes[i]);
            // }



            
        }

        // display_vector(E,N_el);

        // std::cout  << "------------------------" << N_el <<std::endl;
        Real nrm_inf = KokkosBlas::nrminf(Err);
        // mean_error =  nrm_inf; 

        Real nrm_tot = std::sqrt(KokkosBlas::nrm1(E));
        // Real nrm_tot =KokkosBlas::nrm1(E);
        mean_error =  nrm_tot;

        // std::cout << 0.15*nrm_inf  << std::endl;
        //N_el;

        for (int k = 0; k < N_el; k++){
            if ( Err(k) >= 0.0009*nrm_inf ){ //
                // std::cout << k << std::endl;
                elements_to_refine.push_back(k);
            }
        }

        

        if (Dim != 4){
            VTUMeshWriter<Mesh<Dim,ManifoldDim>> w;
            w.write_vtu(path, mesh, Err, true);

        }
       
        
        


    }





}



#endif //ST_RECGRAD_HPP 