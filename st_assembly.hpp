
#ifndef ST_ASSEMBLY_HPP
#define ST_ASSEMBLY_HPP

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>

#include "st_element.hpp"
#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include <err.h>
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv_spec.hpp"
#include <Kokkos_Core.hpp>
#include <KokkosSparse_spmv.hpp>
#include<KokkosBlas1_axpby.hpp>
#include<KokkosBlas1_mult.hpp>
#include<KokkosBlas1_dot.hpp>
#include<KokkosBlas1_scal.hpp>
#include<KokkosBlas1_nrm2.hpp>
#include<KokkosSparse_spadd.hpp>


namespace mars{
    using KokkosVector = Kokkos::View<Real*>;
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;

    template<Integer Dim, Integer ManifoldDim>
    class Assembler{
        public:
        virtual ~Assembler()  {};

        virtual void assemble(const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw)  =  0; // implementato in maniera obbligatoria  se creo una sottoclasse (metodo PURE VIRTUAL)  
        virtual void assemble_st(const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw) = 0;
    };
    
    template<Integer Dim, Integer ManifoldDim>
    class MassMatrixAssembler: public Assembler {
        public: 
        MassMatrixAssembler(const FE<Dim,ManifoldDim> &element, Matrix<Real,ManifoldDim+1, ManifoldDim+1> &mass)
        : element(element),
        mass(mass)
        {}


        // void assemble(FE<Dim, ManifoldDim> &element, const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw, Matrix<Real,ManifoldDim+1, ManifoldDim+1> &mass) override{
            void assemble(const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw) override{
                int n = qp.size(); 
                mass.zero();
                for(int k=0; k<n; k++){
                    element.init_fun(qp[k]); // modify the state of the object element, in particular the attribute basis function 
                    const Real q_new = qw[k]*element.ref_measure;
                    for(int i=0; i<=ManifoldDim; i++){
                        for(int j=0; j<=ManifoldDim; j++){
                                mass(i,j) += element.det_J*q_new*element.fun[j]*element.fun[i];
                        
                    }
                }

            }
        }


        // void assemble_st(FE<Dim, ManifoldDim> &element, const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw, Matrix<Real,ManifoldDim+1, ManifoldDim+1> &mass) override {
            void assemble_st(const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw) override {

                int n = qp.size(); 
                mass.zero();
                for(int k=0; k<n; k++){
                    element.init_fun(qp[k]); // modify the state of the object element, in particular the attribute basis function 
                    const Real q_new = qw[k]*element.ref_measure;
                    for(int i=0; i<=ManifoldDim; i++){
                        for(int j=0; j<=ManifoldDim; j++){
                                mass(i,j) += element.det_J*q_new*element.fun[j]*element.grad[i][1];
                        
                    }
                }

            }
        }

        private:
        Matrix<Real,ManifoldDim+1, ManifoldDim+1> &mass;
        FE<Dim, ManifoldDim> &element;

    }; 

    template<Integer Dim, Integer ManifoldDim>
    class StiffnessMatrixAssembler final: public Assembler{
        public: 
        StiffnessMatrixAssembler(const FE<Dim,ManifoldDim> &element, Matrix<Real,ManifoldDim+1, ManifoldDim+1> &stiffness)
        : element(element),
        stiffness(stiffness)
        {}

        void assemble(const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw) override {
                stiffness.zero();
                element.init_grad();  
                int n = qw.size(); 
                for(int k = 0; k<n; k++){
                    const Real weight = qw[k]*(element.measure);
                    // std::cout << weight << std::endl;
                    for(int i=0; i<=ManifoldDim; i++){
                        for(int j=0; j<=ManifoldDim; j++){
                            // element.grad[j].describe(std::cout);
                            // element.grad[i].describe(std::cout);
                            stiffness(i,j) += weight * dot(element.grad[j],element.grad[i]); //dot_product with q_new here 
                        
                    }
                }
                


            }
        }


        void assemble_st(const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw) override {
                stiffness.zero();
                element.init_grad();  
                element.second_der();
                int n = qw.size(); 
                for(int k = 0; k<n; k++){
                    element.init_fun(qp[k]);
                    const Real weight = qw[k]*(element.measure);
                    // std::cout << weight << std::endl;
                    for(int i=0; i<=ManifoldDim; i++){
                        for(int j=0; j<=ManifoldDim; j++){
                            // element.grad[i].describe(std::cout);
                            // element.grad[j].describe(std::cout);
                            stiffness(i,j) += weight * (element.grad[j][1] * element.grad[i][1] + element.grad[j][0] * element.grad_sp[i]); //dot_product with q_new here 
                        
                    }
                }
                


            }
        }

        private:
        SparseMatrix &stiffness;
        FE<Dim, ManifoldDim> &element;

    };


} 
#endif //ST_ASSEMBLY_HPP
