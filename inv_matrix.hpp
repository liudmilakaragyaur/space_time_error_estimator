#ifndef INV_MATRIX_HPP
#define INV_MATRIX_HPP 

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <array>
#include <initializer_list>
#include <cmath>

#include "mars_base.hpp"
#include "mars_vector.hpp"
#include "mars_matrix.hpp"

namespace mars {


    template<typename T>
    inline T det(const std::array<T, 4> &mat)
    {
        return mat[0] * mat[3] - mat[2] * mat[1];
    }

    template<typename T>
    inline T det(const std::array<T, 9> &mat)
    {
        return mat[0] * (mat[4] * mat[8] - mat[5]*mat[7]) - mat[1]  * (mat[3]* mat[8] - mat[6]*mat[5]) + mat[2]  * (mat[3]* mat[7] - mat[4]*mat[6]);
    }

    template<typename T>
    inline T det(const std::array<T, 16> &mat)
    {
        return  mat[3] * mat[6] * mat[9] * mat[12] - mat[2] * mat[7] * mat[9] * mat[12] -
                mat[3] * mat[5] * mat[10] * mat[12] + mat[1] * mat[7] * mat[10] * mat[12] +
                mat[2] * mat[5] * mat[11] * mat[12] - mat[1] * mat[6] * mat[11] * mat[12] -
                mat[3] * mat[6] * mat[8] * mat[13] + mat[2] * mat[7] * mat[8] * mat[13] +
                mat[3] * mat[4] * mat[10] * mat[13] - mat[0] * mat[7] * mat[10] * mat[13] -
                mat[2] * mat[4] * mat[11] * mat[13] + mat[0] * mat[6] * mat[11] * mat[13] +
                mat[3] * mat[5] * mat[8] * mat[14] - mat[1] * mat[7] * mat[8] * mat[14] -
                mat[3] * mat[4] * mat[9] * mat[14] + mat[0] * mat[7] * mat[9] * mat[14] +
                mat[1] * mat[4] * mat[11] * mat[14] - mat[0] * mat[5] * mat[11] * mat[14] -
                mat[2] * mat[5] * mat[8] * mat[15] + mat[1] * mat[6] * mat[8] * mat[15] +
                mat[2] * mat[4] * mat[9] * mat[15] - mat[0] * mat[6] * mat[9] * mat[15] -
                mat[1] * mat[4] * mat[10] * mat[15] + mat[0] * mat[5] * mat[10] * mat[15];
    }

    template<typename T>
    bool invert(const std::array<T, 4> &mat, std::array<T, 4> &mat_inv, const T &det)
    {
    
        mat_inv[0] =  mat[3]/det;
        mat_inv[1] = -mat[1]/det;
        mat_inv[2] = -mat[2]/det;
        mat_inv[3] =  mat[0]/det;
        return true;
    }

    template<typename T>
    bool invert(const std::array<T, 4> &mat, std::array<T, 4> &mat_inv)
    {
        return invert(mat, mat_inv, det(mat));
    }

    template<typename T>
    bool invert(const std::array<T, 9> &mat, std::array<T, 9> &mat_inv, const T &det)
    {
        assert(det != 0.);
        
        if(det == 0.) {
            return false;
        }

        mat_inv[0] = ( mat[4] * mat[8] - mat[5] * mat[7] )/det;
        mat_inv[1] = ( mat[2] * mat[7] - mat[1] * mat[8] )/det;
        mat_inv[2] = ( mat[1] * mat[5] - mat[2] * mat[4] )/det;
        mat_inv[3] = ( mat[5] * mat[6] - mat[3] * mat[8] )/det;
        mat_inv[4] = ( mat[0] * mat[8] - mat[2] * mat[6] )/det;
        mat_inv[5] = ( mat[2] * mat[3] - mat[0] * mat[5] )/det;
        mat_inv[6] = ( mat[3] * mat[7] - mat[4] * mat[6] )/det;
        mat_inv[7] = ( mat[1] * mat[6] - mat[0] * mat[7] )/det;
        mat_inv[8] = ( mat[0] * mat[4] - mat[1] * mat[3] )/det;
        return true;
    }

    template<typename T>
    bool invert(const std::array<T, 9> &mat, std::array<T, 9> &mat_inv)
    {
        return invert(mat, mat_inv, det(mat));
    }

    template<typename T>
    bool invert(const std::array<T, 16> &mat, std::array<T, 16> &mat_inv, const T &det)
    {
        // std::cout << det << std::endl;
        assert(det != 0.);
        
        if(det == 0.) {
            return false;
        }

        mat_inv[0] = (mat[5]  * mat[10] * mat[15] - 
                    mat[5]  * mat[11] * mat[14] - 
                    mat[9]  * mat[6]  * mat[15] + 
                    mat[9]  * mat[7]  * mat[14] +
                    mat[13] * mat[6]  * mat[11] - 
                    mat[13] * mat[7]  * mat[10]) / det;

        mat_inv[4] = (-mat[4]  * mat[10] * mat[15] + 
                mat[4]  * mat[11] * mat[14] + 
                mat[8]  * mat[6]  * mat[15] - 
                mat[8]  * mat[7]  * mat[14] - 
                mat[12] * mat[6]  * mat[11] + 
                mat[12] * mat[7]  * mat[10])/det;

        mat_inv[8] = (mat[4]  * mat[9] * mat[15] - 
                mat[4]  * mat[11] * mat[13] - 
                mat[8]  * mat[5] * mat[15] + 
                mat[8]  * mat[7] * mat[13] + 
                mat[12] * mat[5] * mat[11] - 
                mat[12] * mat[7] * mat[9])/det;

        mat_inv[12] = (-mat[4]  * mat[9] * mat[14] + 
                mat[4]  * mat[10] * mat[13] +
                mat[8]  * mat[5] * mat[14] - 
                mat[8]  * mat[6] * mat[13] - 
                mat[12] * mat[5] * mat[10] + 
                mat[12] * mat[6] * mat[9])/det;

        mat_inv[1] = (-mat[1]  * mat[10] * mat[15] + 
                mat[1]  * mat[11] * mat[14] + 
                mat[9]  * mat[2] * mat[15] - 
                mat[9]  * mat[3] * mat[14] - 
                mat[13] * mat[2] * mat[11] + 
                mat[13] * mat[3] * mat[10])/det;

        mat_inv[5] = (mat[0]  * mat[10] * mat[15] - 
                mat[0]  * mat[11] * mat[14] - 
                mat[8]  * mat[2] * mat[15] + 
                mat[8]  * mat[3] * mat[14] + 
                mat[12] * mat[2] * mat[11] - 
                mat[12] * mat[3] * mat[10])/det;

        mat_inv[9] = (-mat[0]  * mat[9] * mat[15] + 
                mat[0]  * mat[11] * mat[13] + 
                mat[8]  * mat[1] * mat[15] - 
                mat[8]  * mat[3] * mat[13] - 
                mat[12] * mat[1] * mat[11] + 
                mat[12] * mat[3] * mat[9])/det;

        mat_inv[13] = (mat[0]  * mat[9] * mat[14] - 
                mat[0]  * mat[10] * mat[13] - 
                mat[8]  * mat[1] * mat[14] + 
                mat[8]  * mat[2] * mat[13] + 
                mat[12] * mat[1] * mat[10] - 
                mat[12] * mat[2] * mat[9])/det;

        mat_inv[2] = (mat[1]  * mat[6] * mat[15] - 
                mat[1]  * mat[7] * mat[14] - 
                mat[5]  * mat[2] * mat[15] + 
                mat[5]  * mat[3] * mat[14] + 
                mat[13] * mat[2] * mat[7] - 
                mat[13] * mat[3] * mat[6])/det;

        mat_inv[6] = (-mat[0]  * mat[6] * mat[15] + 
                mat[0]  * mat[7] * mat[14] + 
                mat[4]  * mat[2] * mat[15] - 
                mat[4]  * mat[3] * mat[14] - 
                mat[12] * mat[2] * mat[7] + 
                mat[12] * mat[3] * mat[6])/det;

        mat_inv[10] = (mat[0]  * mat[5] * mat[15] - 
                mat[0]  * mat[7] * mat[13] - 
                mat[4]  * mat[1] * mat[15] + 
                mat[4]  * mat[3] * mat[13] + 
                mat[12] * mat[1] * mat[7] - 
                mat[12] * mat[3] * mat[5])/det;

        mat_inv[14] = (-mat[0]  * mat[5] * mat[14] + 
                mat[0]  * mat[6] * mat[13] + 
                mat[4]  * mat[1] * mat[14] - 
                mat[4]  * mat[2] * mat[13] - 
                mat[12] * mat[1] * mat[6] + 
                mat[12] * mat[2] * mat[5])/det;

        mat_inv[3] = (-mat[1] * mat[6] * mat[11] + 
                mat[1] * mat[7] * mat[10] + 
                mat[5] * mat[2] * mat[11] - 
                mat[5] * mat[3] * mat[10] - 
                mat[9] * mat[2] * mat[7] + 
                mat[9] * mat[3] * mat[6])/det;

        mat_inv[7] = (mat[0] * mat[6] * mat[11] - 
                mat[0] * mat[7] * mat[10] - 
                mat[4] * mat[2] * mat[11] + 
                mat[4] * mat[3] * mat[10] + 
                mat[8] * mat[2] * mat[7] - 
                mat[8] * mat[3] * mat[6])/det;

        mat_inv[11] = (-mat[0] * mat[5] * mat[11] + 
                mat[0] * mat[7] * mat[9] + 
                mat[4] * mat[1] * mat[11] - 
                mat[4] * mat[3] * mat[9] - 
                mat[8] * mat[1] * mat[7] + 
                mat[8] * mat[3] * mat[5])/det;

        mat_inv[15] = (mat[0] * mat[5] * mat[10] - 
                mat[0] * mat[6] * mat[9] - 
                mat[4] * mat[1] * mat[10] + 
                mat[4] * mat[2] * mat[9] + 
                mat[8] * mat[1] * mat[6] - 
                mat[8] * mat[2] * mat[5])/det;


    
    return true;
    }

    template<typename T>
    bool invert(const std::array<T, 16> &mat, std::array<T, 16> &mat_inv)
    {   
        return invert(mat, mat_inv, det(mat));
    }



}

#endif //INV_MATRIX_HPP