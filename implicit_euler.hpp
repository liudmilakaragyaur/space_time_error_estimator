#ifndef EULER_HPP
#define EULER_HPP

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <err.h>

#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include "precon_conjugate_grad.hpp"
#include "vtu_writer.hpp"
#include "Kokkos_Core.hpp"
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv.hpp"
#include "KokkosSparse_spmv_spec.hpp"
#include "KokkosBlas1_axpby.hpp"
#include "KokkosBlas1_mult.hpp"
#include "KokkosBlas1_dot.hpp"
#include "KokkosBlas1_scal.hpp"
#include "KokkosSparse_spadd.hpp"
#include "KokkosBlas1_nrm2.hpp"



namespace mars{
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    using KokkosVector = Kokkos::View<Real*>;

     
    template<Integer Dim, Integer ManifoldDim, class F> 

    void implicit_euler(
    const Mesh<Dim,ManifoldDim> &mesh,
    const SparseMatrix &A, 
    const SparseMatrix &M, 
    KokkosVector &x_0, //const 
    // const KokkosVector &Mb,
    KokkosVector &result, //const 
    const std::vector<Real> &time, 
    std::vector<bool> &node_is_boundary,
    F f){

        Integer N =  A.numRows();
        Integer T = time.size();
        

        // Real dt = time[1] - time[0];

        Kokkos::View<Real*> x_new("x_new", N );
        Kokkos::View<Real*> init_guess("init_guess", N );
        Kokkos::View<Real*> c1("Mu", N );
        // Kokkos::View<Real*> c1b("MuI", N );
        Kokkos::View<Real*> c3("Op_c1", N ); // (M+dt*A) \ M * u_sol(:,i-1)

        Kokkos::View<Real*> b("rhs", N ); // current rhs 
        KokkosVector Mb("Mb", N ); // M*b
        Kokkos::View<Real*> b1("rhs_dt", N ); // current rhs * dt

        // assert(time.size() == T);

        for (int j=1; j<T; ++j){

            for (int k=0;  k< N; k++){
                assert(k < mesh.n_nodes());
                if(!node_is_boundary[k]) {
                    b(k) =  f(time[j], mesh.point(k));
                } else {
                    b(k) = 0.0;
                }                
            }

            Real dt = time[j] - time[j-1];

            KokkosSparse::spmv("N", 1.0, M, b, 0.0, Mb); // M  * rhs
            KokkosSparse::spmv("N", 1.0, M, x_0, 0.0, c1); //  M * u_old

            // for (int k=0;  k< N; k++){
            //     c1b(k) =  0;
            // }

            // KokkosBlas::axpy(1.0, c1, c1b);
            Real num_iter;
            preconCG(A, M, c1, node_is_boundary, dt,init_guess, c3, num_iter); 

            KokkosBlas::scal(b1,dt,Mb);
            preconCG(A, M, b1, node_is_boundary, dt,init_guess, x_new, num_iter); 

            KokkosBlas::axpy(1.0, c3, x_new);
            Kokkos::deep_copy(x_0, x_new);

            // VTUMeshWriter<Mesh2> w;
            // std::string path = "/Users/liudmilakaragyaur/code/space_time_error_estimator/result_SP" + std::to_string(j) + ".vtu";
            // w.write_vtu(path, mesh, x_new);

        }

        Kokkos::deep_copy(result, x_new);


    }

} 

#endif //EULER_HPP