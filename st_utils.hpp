#ifndef ST_UTILS_HPP
#define ST_UTILS_HPP

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>

// #include "sparse_matrix.hpp"
#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include "vtu_writer.hpp"
#include "sparse_matrix.hpp"
#include "Kokkos_Core.hpp"
#include "KokkosSparse_CrsMatrix.hpp"
#include <err.h>


namespace mars {

    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    using KokkosVector = Kokkos::View<Real*>;


    template<Integer Dim, Integer ManifoldDim>
    void boundary_nodes(const Mesh<Dim,ManifoldDim> &mesh,std::vector<bool> &node_is_boundary, bool flag = false, Integer side_tag = -1){
        for (int k = 0; k < mesh.n_elements(); k++) {
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(k);
            Simplex<Dim, ManifoldDim-1> side_i;
            if (flag == true){
                // please specify the side with no B
                if (mesh.is_boundary(k)) {  
                    for (int i=0; i< n_sides(simplex); i++) {
                        if (simplex.side_tags[i] !=side_tag) { //!= 4
                            if (mesh.is_boundary(k, i)){
                                simplex.side(i,side_i);
                                for(int j = 0; j < side_i.n_nodes(); ++j) {
                                const Integer v = side_i.nodes[j];
                                // std::cout << mesh.point(side_i.nodes[j]) << std::endl;
                                node_is_boundary[v] = true;
                                }
                            }

                        }
                    }
       
                }   

            }else{
                // the whole boundary 
                if (mesh.is_boundary(k)) {  
                    for (int i=0; i< n_sides(simplex); i++) {
                        if (mesh.is_boundary(k, i)){
                            simplex.side(i,side_i);
                            for(int j = 0; j < side_i.n_nodes(); ++j) {
                            const Integer v = side_i.nodes[j];
                            node_is_boundary[v] = true;
                            }
                        }

                        
                    }
       
                }   



            }
            

        }
            
    }

    template<Integer Dim, Integer ManifoldDim>
    void boundary_sides(const Mesh<Dim,ManifoldDim> &mesh, int &id_elem, Vector<bool, ManifoldDim+1> &edge_is_boundary, Integer init_cond_side, Integer bound_cond_side ){
        Simplex<Dim, ManifoldDim-1> side_i;
        const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(id_elem);
        if (mesh.is_boundary(id_elem)) { 
            for (int i=0; i< n_sides(simplex); i++) {
                if (mesh.is_boundary(id_elem, i)){  // if (simplex.side_tags[i] == init_cond_side || simplex.side_tags[i] == bound_cond_side){ 
                    edge_is_boundary[i] = true;
                } else {
                    edge_is_boundary[i] = false;
                }
            }
        }
    }


    template<Integer Dim, Integer ManifoldDim>
    // std::function<Real(const Vector<Real, 2> &x)>
    void assign_boundary_cond(const Mesh<Dim,ManifoldDim> &mesh, SparseMatrix &A, KokkosVector &b, std::vector<bool> &node_is_boundary){
        std::vector<Vector<Real,ManifoldDim>> points = mesh.points();
        for (Integer k=0; k < mesh.n_nodes(); k++){
            if (node_is_boundary[k] == 1){
                b(k) = 0; //points[k][ManifoldDim-1];
                auto row = A.row(k); 
                for(Integer i = 0; i < row.length; ++i) {
                    auto j = row.colidx(i);
                    if (k==j){
                        row.value(i) = 1.0;
                    } else {
                        row.value(i) = 0.0;
                    }
                }

            }

        }
    }
    

    template<Integer Dim, Integer ManifoldDim,  class F>
    void assign_init_cond(const Mesh<Dim,ManifoldDim> &mesh, F init_cond, KokkosVector &rhs){
        for (int k = 0; k< mesh.n_nodes(); ++k){

            if (mesh.point(k)[ManifoldDim-1] == 0){   
                rhs(k) = init_cond(mesh.point(k));
            }
        
        }   

        
    }


    template<Integer Dim, Integer ManifoldDim>
    void max_diam(Mesh<Dim,ManifoldDim> &mesh, Real &max_d){
        std::vector<Real> diam_elem;
        FE<Dim,ManifoldDim> element;
        
        for (int k = 0; k< mesh.n_elements(); ++k){
            
            element.elem_diam(mesh,k);
            diam_elem.push_back(element.diam);
        
        }   

        Real max1 = diam_elem[0];
        for (int j = 1; j  < diam_elem.size(); j++ ){
            Real max2 = std::max(diam_elem[j], max1); 
            max1 = max2;
        }
        max_d = max1;

    }


    template<Integer Dim, Integer ManifoldDim>
    void min_diam(Mesh<Dim,ManifoldDim> &mesh, Real &min_d){
        std::vector<Real> diam_elem;
        FE<Dim,ManifoldDim> element;
        
        for (int k = 0; k< mesh.n_elements(); ++k){
            
            element.elem_diam(mesh,k);
            diam_elem.push_back(element.diam);
        
        }   

        Real min1 = diam_elem[0];
        for (int j = 1; j  < diam_elem.size(); j++ ){
            Real min2 = std::min(diam_elem[j], min1); 
            min1 = min2;
        }
        min_d = min1;

    }

    // template<Integer Dim, Integer ManifoldDim>
    // void compute_exact_error(
    //     const Mesh<Dim,ManifoldDim> &mesh, 
    //     const KokkosVector &x, 
    //     const KokkosVector &y,
    //     Real &tot_error){
        
    //     std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/exact_error.vtu";
    //     std::vector<Vector<Real, Dim>> qp;
    //     std::vector<Real> qw;

    //     const size_t N_el = mesh.n_elements();
       

    //     Quadrature<1,Dim> q4;

    //     qp = q4.get_points();
    //     qw = q4.get_weights();


    //     KokkosVector exact_error("exact_error", N_el); 
    //     FE<Dim,ManifoldDim> element;

    //     tot_error = 0;
    //     for (int k = 0; k< mesh.n_elements(); ++k){
    //         element.init_jacobian(mesh, k); 
    //         const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(k);
    //         std::array<Integer, ManifoldDim+1> nodes = mesh.elem(k).nodes;

    //         Vector<Real, ManifoldDim+1>  coeff1; 
    //         Vector<Real, ManifoldDim+1>  coeff2; 
    //         coeff1.zero();
    //         coeff2.zero();

    //         for (int i = 0; i<ManifoldDim+1;  i++){
    //             coeff1(i) = x(nodes[i]);
    //             coeff2(i) = y(nodes[i]);
    //         }

            
    //         Real error = 0;
    //         for (int j = 0; j <  qw.size(); j++){

    //             element.init_fun(qp[j]); 
    //             const Real q_new = qw[j] * element.ref_measure;

    //             Real diff = 0;
    //             for (int i = 0; i <  ManifoldDim+1; i++){
    //                 diff += element.fun[i] * coeff1[i] - element.fun[i] *coeff2[i];
    //             }

    //             error += element.det_J * q_new * diff * diff;

    //         }

    //         exact_error(k)  = error;
    //         tot_error += error;

    //         // std::cout << exact_error(k) << std::endl;
        
    //     }   

        
    //     tot_error =  std::sqrt(KokkosBlas::nrm1(exact_error));
    //     //std::sqrt(tot_error);

    //     if (Dim != 4){
    //         VTUMeshWriter<Mesh<Dim,ManifoldDim>> w;
    //         w.write_vtu(path_error, mesh, exact_error, true);
    //     }
        
    // }


template<Integer Dim, Integer ManifoldDim, class F>
    void compute_exact_error(
        const Mesh<Dim,ManifoldDim> &mesh, 
        F fun_exact,
        const KokkosVector &y,
        Real &tot_error){
        
        std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/exact_error.vtu";
        std::vector<Vector<Real, Dim>> qp;
        std::vector<Real> qw;

        const size_t N_el = mesh.n_elements();
        const size_t N = mesh.n_nodes();

        // std::function<Real(const Vector<Real,Dim>&)>

        // Quadrature<5,Dim> q4; //2+3D

        // Quadrature<256,Dim> q4; //4D
        // Quadrature<27,Dim> q4; //3D
        Quadrature<4,Dim> q4; //2D

        qp = q4.get_points();
        qw = q4.get_weights();


        KokkosVector exact_error("exact_error", N_el); 
        FE<Dim,ManifoldDim> element;

        tot_error = 0;
        for (int k = 0; k< mesh.n_elements(); ++k){
            element.init_jacobian(mesh, k); 
            element.points(mesh,k);
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(k);
            std::array<Integer, ManifoldDim+1> nodes = mesh.elem(k).nodes;

            Vector<Real, ManifoldDim+1>  coeff2; 
            coeff2.zero();

            for (int i = 0; i<ManifoldDim+1;  i++){
                
                coeff2(i) = y(nodes[i]);
            }

            
            Real error = 0;
            for (int j = 0; j <  qw.size(); j++){

                element.init_fun(qp[j]); 
                const Real q_new = qw[j] * element.ref_measure;

                Vector<Real, ManifoldDim> qp_new; 
                qp_new.zero();
                qp_new = element.J * qp[j] + mesh.point(nodes[0]);

                Real diff = 0;
                for (int i = 0; i <  ManifoldDim+1; i++){
                    diff += element.fun[i] * coeff2[i];

                }
                Real diff2 = fun_exact(qp_new) - diff;
                error += element.det_J * q_new * diff2 * diff2;

            }

            exact_error(k)  = error;
            // tot_error += error;

            // std::cout << exact_error(k) << std::endl;
        
        }   

        // for(int i=0;i<N;i++){
        //     std::cout<<exact_error(i)<<std::endl;
        // }
        tot_error =  std::sqrt(KokkosBlas::nrm1(exact_error));
        // tot_error = std::sqrt(tot_error);

        if (Dim != 4){
            VTUMeshWriter<Mesh<Dim,ManifoldDim>> w;
            w.write_vtu(path_error, mesh, exact_error, true);
        }
        
    }


    // linspace from https://gist.github.com/lorenzoriano/5414671
    template <typename T>
    std::vector<T> linspace(T a, T b, size_t N) {
        T h = (b - a) / (N-1);
        std::vector<T> dt(N);
        typename std::vector<T>::iterator x;
        T val;
        for (x = dt.begin(), val = a; x != dt.end(); ++x, val += h)
            *x = val;
        return dt;
    }


    template<typename T, Integer Rows, Integer Cols>
    inline Vector<T, Rows> operator * (const Matrix<T, Rows, Cols> &mat, const Vector<T, Cols> &x)
    {
        Vector<T, Rows> res;
        for(Integer i = 0; i < Rows; ++i) {
            res[i] = 0;
            for(Integer j = 0; j < Cols; ++j) {
                res[i] += mat(i, j) * x[j];
            }
        }

        return res;
    }



    template<typename T, Integer Rows, Integer Cols>
    inline Vector<T, Rows> operator * (const Vector<T, Cols> &x, const Matrix<T, Rows, Cols> &mat)
    {
        Vector<T, Rows> res;
        for(Integer i = 0; i < Rows; ++i) {
            res[i] = 0;
            for(Integer j = 0; j < Cols; ++j) {
                res[i] += x[j] * mat(j,i); 
            }
        }

        return res;
    }




}


#endif //ST_UTILS_HPP