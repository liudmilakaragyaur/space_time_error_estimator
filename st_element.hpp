#include <cassert>
#include <cstdio>
#include "mars_simplex.hpp" 
#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_static_math.hpp"
#include "mars_mesh.hpp"
#include "inv_matrix.hpp"
#include <Kokkos_Core.hpp>
#include "KokkosSparse_CrsMatrix.hpp"


namespace mars {

    template<Integer Dim, Integer ManifoldDim>
    class FE  {
        public: 

        FE()
        : ref_measure(1./Factorial<ManifoldDim>::value),
        ref_grad_(init_grad_ref())
        {}

        using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
        using KokkosVector = Kokkos::View<Real*>;

        void init_fun(const Vector<Real,ManifoldDim> &x)
        {   
            
            fun[0] = 1; 
            for (int i = 0; i<ManifoldDim; i++){
                fun[0] -= x[i];
                fun[i+1] = x[i];
            }
            
        }  

        inline const std::array<Vector<Real, ManifoldDim>, ManifoldDim+1>  init_grad_ref(){

            for (int k = 0; k < ManifoldDim; k++){
                ref_grad_[0][k] = -1;
            }
            for (int j = 1; j < ManifoldDim+1; j++){
                for (int i = 0; i < ManifoldDim; i++){
                    if (j==(i+1)){
                        ref_grad_[j][i] =  1;
                    } else {
                        ref_grad_[j][i] =  0;
                    }
                }
            }
            

            return  ref_grad_;

        }

        // inline std::array<Vector<Real, ManifoldDim>, ManifoldDim+1>  ref_grad() const {
        //     return refgrad_;
        // }

        void init_grad()
        {
            J_inv_tr = transpose(J_inv);
            for (int j = 0; j < ManifoldDim+1; j++) {
                grad[j] = J_inv_tr *  ref_grad_[j]; 
            }

 
        }  

        void points(const Mesh<Dim,ManifoldDim> &mesh, const Integer id_elem) { 
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(id_elem);

            std::array<Integer, ManifoldDim+1> nodes_elem = mesh.elem(id_elem).nodes; 

            // std::array<Vector<Real, ManifoldDim>, ManifoldDim+1> points_on_ref;

            for (int i = 0; i< ManifoldDim+1; i++){
                points_on_ref[i] = mesh.point(nodes_elem[i]);
                
            }



        }


        void second_der(){
            Vector<Real,ManifoldDim+1> grad_sp; 
            grad_sp.zero(); //this is ok in case of piecewise linear test functions (P1)

        }

        void init_jacobian(const Mesh<Dim,ManifoldDim> &mesh, const Integer id_elem){
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(id_elem);
            
            jacobian(simplex, mesh.points(), J); 
            det_J = std::abs(det(J));
            assert(det_J > 0);
            if (!invert(J.values, J_inv.values)){
                std::cerr << "Error: jacobian not invertible" << std::endl; 
            }
            measure = det_J*ref_measure;
        }

        void init_jacobian_edge(const Mesh<Dim,ManifoldDim> &mesh, const Integer id_elem, const Integer id_edge){
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(id_elem);


            Simplex<Dim, ManifoldDim-1> side_el; 
            simplex.side(id_edge,side_el);

            jacobian(side_el, mesh.points(), J_edge); 

            JtJ = transpose(J_edge)  * J_edge;

            det_J_edge = 0.0;
            det_J_edge = std::sqrt(det(JtJ));


        }

        void fix_volume(std::array<Integer, ManifoldDim+1> &nodes){
                Integer n1 = nodes[1];
                nodes[1] = nodes[2];
                nodes[2] = n1;

        }

        void sum(Mesh<Dim, ManifoldDim> &mesh, const std::vector<Vector<Real, ManifoldDim>> &qp, const Vector<Real, ManifoldDim+1> &coeff, Vector<Real,ManifoldDim> &result ) {
            int N = ManifoldDim+1;
            Vector<Real,ManifoldDim> result0; 
            result0.zero();
            for(int k = 0; k<N; k++){
                result0 += coeff(k) * ref_grad_[k];

            }
            
            result = result0 * J_inv;

            // for (int i = 0; i< ManifoldDim; i++){
            //     result[i] = result0[0] * J_inv(0,i) + result0[1] * J_inv(1,i); 
            // }

            // for (int i = 0; i< ManifoldDim; i++){
            //     result[i] = result0[0] * J_inv(0,i) + result0[1] * J_inv(1,i) + result0[2] * J_inv(2,i); 
            // }
            

        }



        void interpolate(Mesh<Dim, ManifoldDim> &mesh, const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw, const Vector<Real, ManifoldDim+1> &coeff, Real &result ) {
            int N = ManifoldDim+1;
            for(int k = 0; k<qp.size(); k++){
                init_fun(qp[k]);
                for (int i = 0; i < N; i++){ 
                    result += coeff(i) * fun[i];
                }
                
            }

        }


        void integrate(Mesh<Dim, ManifoldDim> &mesh, const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw, const Real &coeff,  Vector<Real,ManifoldDim+1> &result) {
            int N = ManifoldDim+1;
            int n = qw.size();
            result.zero();

            for(int k = 0; k < n; k++){
                init_fun(qp[k]);
                Real q_new = qw[k] *  ref_measure;
                for (int i = 0; i < N; i++){ 
                    result(i) += q_new * det_J * coeff * fun[i];
                }
            }

        }

        

        void elem_diam(Mesh<Dim, ManifoldDim> &mesh, int &id_elem) {
            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(id_elem);
            std::array<Integer, ManifoldDim+1> &nodes = mesh.elem(id_elem).nodes;

            Integer N_edge = n_edges(simplex);

            std::vector<Real> edge_lengths;

            for (int i =0; i  < N_edge; i++ ){
                Integer v1; 
                Integer v2;
                simplex.edge(i, v1, v2);
                Vector<Real,ManifoldDim> diff = mesh.point(v2) - mesh.point(v1);
                Real dist = diff.norm();
                edge_lengths.push_back(dist);

            }
            
            Real max1 = edge_lengths[0];
            for (int j = 1; j  < edge_lengths.size(); j++ ){
               Real max2 = std::max(edge_lengths[j], max1); 
               max1 = max2;
            }

            
            diam = max1;


        }

        

        

        void  parent_to_child(const Mesh<Dim,ManifoldDim> &mesh, const Integer id_child, const KokkosVector &sol_old, KokkosVector &sol_new){

            const Simplex<Dim, ManifoldDim> &simplex_child = mesh.elem(id_child);
            const std::array<Integer, ManifoldDim + 1> &nodes_child = simplex_child.nodes;
            
            Integer id_parent = simplex_child.parent_id;
            
            const Simplex<Dim, ManifoldDim> &simplex_parent = mesh.elem(id_parent);
            const std::array<Integer, ManifoldDim + 1> &nodes_parent = simplex_parent.nodes;

            init_jacobian(mesh, id_parent);
            std::array<Vector<Real, ManifoldDim>, ManifoldDim+1> points_child_on_ref;

            

            for (int i = 0; i < ManifoldDim+1; i++){
                points_child_on_ref[i] = J_inv * (mesh.point(nodes_child[i]) - mesh.point(nodes_parent[0]));
            }


            Vector<Real, ManifoldDim+1> coeff;
            coeff.zero();
            
            for (int k = 0; k < ManifoldDim+1; k++){
                // assert(nodes_parent[k] <= sol_old.extent(0));
                // std::cout << "node_par: " << nodes_parent[k]<< std::endl;
                
                if (nodes_parent[k] > sol_old.extent(0)){
                    coeff(k) = sol_new(nodes_parent[k]);
                    // std::cout << "coeff: " << sol_new(nodes_parent[k])<< std::endl;
                } else {
                    coeff(k) = sol_old(nodes_parent[k]);
                }


                // assert(nodes_parent[k] <= sol_old.extent(0));

            }
            
            


            Vector<Real, ManifoldDim+1> values_child_on_ref;
            values_child_on_ref.zero();

            for (int i=0; i< ManifoldDim+1; i++){
                init_fun(points_child_on_ref[i]);
                for (int j=0; j< ManifoldDim+1; j++){
                    values_child_on_ref(i) += coeff(j) * fun[j];
                }
            }


            for (int k = 0; k < ManifoldDim+1; k++){
                // std::cout << "NODE NEW: " << nodes_child[k] << std::endl;
                sol_new(nodes_child[k]) = values_child_on_ref(k);
                
            } 


        }



        Vector<Real, ManifoldDim+1> fun; 
        Vector<Real, ManifoldDim+1> grad_sp;
        std::array<Vector<Real, Dim>, ManifoldDim+1> grad;
        Matrix<Real, Dim, ManifoldDim> J; 
        Matrix<Real, Dim, ManifoldDim-1> J_edge;
        Matrix<Real, ManifoldDim-1, ManifoldDim-1> JtJ;
        Matrix<Real, ManifoldDim, Dim> J_inv;
        Matrix<Real, Dim, ManifoldDim> J_inv_tr;
        std::array<Integer, ManifoldDim+1> nodes;
        std::array<Vector<Real, ManifoldDim>, ManifoldDim+1> points_on_ref;
        Vector<Real, ManifoldDim+1> values_on_ref;


        Real det_J; 
        Real det_J_edge;
        Real ref_measure; 
        Real measure; 
        Real diam;

        private: 
        std::array<Vector<Real, ManifoldDim>, ManifoldDim+1> ref_grad_;   
        
        

    };

    template<Integer Dim, Integer ManifoldDim>
    class MassMatrixAssembler{
        public: 

        void assemble(FE<Dim, ManifoldDim> &element, 
        const std::vector<Vector<Real, ManifoldDim>> &qp, 
        const std::vector<Real> &qw, 
        Matrix<Real,ManifoldDim+1, ManifoldDim+1> &mass, 
        Real diam,
        Real theta,
        bool space_time = false,
        bool upwind = false
        ){
            int n = qp.size(); 
            mass.zero(); 
            element.init_grad();

            Vector<Real,ManifoldDim+1> time_grad;
            std::array<Vector<Real, ManifoldDim-1>,ManifoldDim+1> space_grad;


            for (int t=0; t < ManifoldDim+1; t++){
                time_grad[t] = element.grad[t][ManifoldDim-1];
                for (int tt = 0; tt < ManifoldDim-1; tt++){
                    space_grad[t][tt] = element.grad[t][tt];
                }
                
            }

           

            for(int k=0; k<n; k++){
                element.init_fun(qp[k]); // modify the state of the object element, in particular the attribute basis function 
                const Real q_new = qw[k] * element.ref_measure;
                for(int i=0; i<=ManifoldDim; i++){
                    for(int j=0; j<=ManifoldDim; j++){
                        if  (space_time){ 
                            // std::cout << element.fun[i] << std::endl;
                            // std::cout << element.fun[j] << std::endl;
                            // mass(i,j) += element.det_J*q_new*element.fun[j]*element.grad[i][1];
                            if (!upwind){
                                // std::cout<< "NO UPWIND" << std::endl;
                                mass(i,j) += element.det_J*q_new*element.fun[j]*element.fun[i];
                            } else {
                                // std::cout<< element.fun[i] + theta * diam * time_grad[i] << std::endl;

                                mass(i,j) += element.det_J*q_new*element.fun[j]*(element.fun[i] + theta * diam * time_grad[i]);
                            }
                            
                            // 

                        } else {
                            // std::cout << "fun " << element.fun[i] << std::endl;
                            // std::cout << "fun " << element.fun[j] << std::endl;
                            mass(i,j) += element.det_J*q_new*element.fun[j]*element.fun[i];

                        }
                            
                    
                    }
                }

            }
        }

    }; 

    template<Integer Dim, Integer ManifoldDim>
    class StiffnessMatrixAssembler{
        public: 

        void assemble(FE<Dim, ManifoldDim> &element, 
        const std::vector<Vector<Real, ManifoldDim>> &qp, 
        const std::vector<Real> &qw, 
        Matrix<Real,ManifoldDim+1, ManifoldDim+1> &stiffness, 
        Real diam,
        Real theta,
        bool space_time = false,
        bool upwind = false
        ){
            stiffness.zero();
            element.init_grad();  
            int n = qw.size(); 

            Vector<Real,ManifoldDim+1> time_grad;
            std::array<Vector<Real, ManifoldDim-1>,ManifoldDim+1> space_grad;
            // std::cout<< "DIAM:" << diam << std::endl;

            for (int t=0; t < ManifoldDim+1; t++){
                time_grad[t] =element.grad[t][ManifoldDim-1];
                for (int tt = 0; tt < ManifoldDim-1; tt++){
                    space_grad[t][tt] = element.grad[t][tt];
                }
                
            }


            for(int k = 0; k<n; k++){
                const Real weight = qw[k]*(element.measure);  //
                element.init_fun(qp[k]);



                for(int i=0; i<=ManifoldDim; i++){
                    for(int j=0; j<=ManifoldDim; j++){
                        if (space_time){
                            // stiffness(i,j) += weight * (element.grad[j][1] * element.fun[i] + element.grad[j][0] * element.grad[i][0]); //dot_product with q_new here 
                            if (!upwind){
                                // std::cout<< "NO UPWIND" << std::endl;
                                stiffness(i,j) += weight * (time_grad[j] * element.fun[i] + dot(space_grad[j],space_grad[i]) ); //dot_product with q_new here 
                            } else {
                                stiffness(i,j) += weight * (time_grad[j] * element.fun[i] + dot(space_grad[j],space_grad[i]) + theta * diam * time_grad[j] * time_grad[i]); //dot_product with q_new here 
                            }
                            
                            //

                        } else {

                            stiffness(i,j) += weight * dot(element.grad[j],element.grad[i]); //dot_product with q_new here 
                        }
                    
                    }
                }
            


            }
        }


        void assemble2(FE<Dim, ManifoldDim> &element, 
        const std::vector<Vector<Real, ManifoldDim>> &qp, 
        const std::vector<Real> &qw, 
        Matrix<Real,ManifoldDim+1, ManifoldDim+1> &stiffness, 
        Real diam,
        Real theta,
        bool space_time = false,
        bool upwind = false
        ){
            stiffness.zero();
            element.init_grad();  
            int n = qw.size(); 

            Vector<Real,ManifoldDim+1> time_grad;
            std::array<Vector<Real, ManifoldDim-1>,ManifoldDim+1> space_grad;
            // std::cout<< "DIAM:" << diam << std::endl;

            for (int t=0; t < ManifoldDim+1; t++){
                time_grad[t] =element.grad[t][ManifoldDim-1];
                for (int tt = 0; tt < ManifoldDim-1; tt++){
                    space_grad[t][tt] = element.grad[t][tt];
                }
                
            }

            for(int k = 0; k<n; k++){
                const Real weight = qw[k]*(element.measure);  //
                element.init_fun(qp[k]);

                Vector<Real, ManifoldDim> qp_new; 
                qp_new = element.J * qp[k] + element.points_on_ref[0];
                Real diff_coeff;
                if (qp_new[0] >= 0.495 & qp_new[0] <= 0.505){
                    // if (qp_new[1] >= 0.2 & qp_new[1] <= 0.7) {
                    
                        diff_coeff = 100;

                    // }

                } else {
                    diff_coeff = 1;
                }

                for(int i=0; i<=ManifoldDim; i++){
                    for(int j=0; j<=ManifoldDim; j++){
                        if (space_time){
                            // stiffness(i,j) += weight * (element.grad[j][1] * element.fun[i] + element.grad[j][0] * element.grad[i][0]); //dot_product with q_new here 
                            if (!upwind){
                                // std::cout<< "NO UPWIND" << std::endl;
                                stiffness(i,j) += weight * (time_grad[j] * element.fun[i] + dot(space_grad[j],space_grad[i]) ); //dot_product with q_new here 
                            } else {
                                stiffness(i,j) += weight * (time_grad[j] * element.fun[i] +  dot(diff_coeff * space_grad[j],space_grad[i]) + theta * diam * time_grad[j] * time_grad[i]); //dot_product with q_new here 
                            }
                            
                            //

                        } else {

                            stiffness(i,j) += weight * dot(element.grad[j],element.grad[i]); //dot_product with q_new here 
                        }
                    
                    }
                }
            


            }
        }

    };




}

