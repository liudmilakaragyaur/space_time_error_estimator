#ifndef ST_CG_HPP
#define ST_CG_HPP

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>

#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include "matrix_free_operator.hpp"
#include "sparse_matrix.hpp"
#include "st_csv_writer.hpp"
#include <err.h>
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv_spec.hpp"
#include <Kokkos_Core.hpp>
#include <KokkosSparse_spmv.hpp>
#include<KokkosBlas1_axpby.hpp>
#include<KokkosBlas1_mult.hpp>
#include<KokkosBlas1_dot.hpp>
#include<KokkosBlas1_scal.hpp>

#include<KokkosBlas1_nrm2.hpp>


//Note on Blas1 Mult. We want to do z[i] = b*z[i] + a*x[i]*y[i] --> use as KokkosBlas::mult(b, z, a, x, y); 
namespace mars{
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    using KokkosVector = Kokkos::View<Real*>;

    void BiCGSTAB(
        Operator &A,
        Operator &P,
        const KokkosVector &b,
        KokkosVector &x_0,
        const size_t &N,
        Integer max_iter,
        double &omega_precon,
        KokkosVector &result,
        Real &num_iter

    ){
        Real TOL =  1.0e-10; //set the tolerance
        std::cout << "Max iter: " << max_iter <<  std::endl;
        //  Initialize vectors 
        Kokkos::View<Real*>r_0("r_0", N );
        Kokkos::View<Real*>r_1("r_1", N );
        Kokkos::View<Real*>r_orig("r_orig", N );
        Kokkos::View<Real*>r0_hat("r0_hat", N );
        Kokkos::View<Real*>x_1("x_1", N );
        Kokkos::View<Real*>p_0("p_0", N );
        Kokkos::View<Real*>p_1("p_1", N );
        Kokkos::View<Real*>v_0("v_0", N );
        Kokkos::View<Real*>v_1("v_1", N );
        Kokkos::View<Real*>y("y", N );
        Kokkos::View<Real*>h("h", N );
        Kokkos::View<Real*>s("s", N );
        Kokkos::View<Real*>t("t", N );
        Kokkos::View<Real*>z("z", N );
        Kokkos::View<Real*>tP("tP", N );

        std::vector<Real> rel_error;
        
        CSVWriter csv;
        // std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/results/csv/error_bcgstab/rel_err_init_zero.csv";
        std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/results/csv2/error_bcgstab/err_70.csv";


        // Initliaze scalars 
        Real alpha = 1; 
        Real beta;
        Real rho_0 = 1; 
        Real rho_1; 
        Real omega_0 = 1; 
        Real omega_1; 

        A.apply(x_0, r_0); // r_0 =  A*x_0
        KokkosBlas::axpy(-1.0, r_0, b); // r_0 = b - A*x_0
        Kokkos::deep_copy (r_0, b);


        // KokkosBlas::axpby(1.0, b, -1.0, r_0); //r_0 = b - A*x_0

        Kokkos::deep_copy (r0_hat, r_0);  // r0_hat = r0;
        Kokkos::deep_copy (r_orig, r_0);
        // display_vector(r_0, N);
        double eps2_d = KokkosBlas::nrm2(b);
        
        int count =  0;
        while (count < max_iter){
            count++;

            rho_1 = KokkosBlas::dot(r0_hat,r_0); //rho_1 = r0_hat' * r0
            beta = (rho_1/rho_0) * (alpha/omega_0); // beta  = (rho_1/rho_0)  * (alpha /omega_0) 

            KokkosBlas::scal(p_1,omega_0,v_0);// p_1  = omega_0 *  v0
            
            KokkosBlas::axpby(1.0, p_0, -1.0, p_1); // p_1 = p_0 -  p_1
            
            KokkosBlas::axpby(1.0, r_0, beta, p_1);  // p1 = r0+ beta *(p0 - omega_0 *v0);
            P.apply(p_1, y); // y = M*r_0

            A.apply(y, v_1); // v_1 = A * y

            Real denom_0 = KokkosBlas::dot(r0_hat,v_1); 
            
            alpha = rho_1 / denom_0; // alpha = rho_1./(r0_hat' * v1);

            KokkosBlas::scal(h,alpha,y); // h = alpha * y
            KokkosBlas::axpby(1.0, x_0, 1.0, h); //h = x0 + h = x0 + alpha * y;
            
            KokkosBlas::scal(s,-alpha,v_1); // s = - alpha * v_1
            KokkosBlas::axpby(1.0, r_0, 1.0, s); //s = r0 + s = r0 - alpha*v_1;

            //  early convergence 
            double eps = KokkosBlas::nrm2(s);
            // std::cout << eps << std::endl;

            rel_error.push_back(eps);
            
            if (eps < TOL){
                Kokkos::deep_copy (x_0, h);
                count =  count - 0.5;
                // terminate the loop
                break;
            }


            P.apply(s,z); // z = M*s
            A.apply(z,t); // t = A * z

            P.apply(t,tP);
            Real numer_1 = KokkosBlas::dot(tP,s); // * omega_precon;
            Real denom_1 = KokkosBlas::dot(tP,t); // * omega_precon;

            // omega_1 =  0;
            omega_1 = numer_1/denom_1;

            // std::cout  << "OMEGA: " <<omega_1 << std::endl;

            KokkosBlas::scal(x_1,omega_1,z); // x1 = omega_1 * z
            KokkosBlas::axpby(1.0, h, 1.0, x_1);  // x1  = h  + x1 = h +  omega_1 * z


            KokkosBlas::scal(r_1,-omega_1,t); // r1 = - omega_1 * t
            KokkosBlas::axpby(1.0, s, 1.0, r_1); //r_1 = s - r1 = s - omega_1*t;
            


            // Check realtive norm 
            double eps2_n = KokkosBlas::nrm2(r_1);
            double eps2 = eps2_n / eps2_d;
            
            if (eps2 < TOL){ //eps2
                Kokkos::deep_copy (x_0, x_1);
                // terminate the loop
                break;
            }

            
            Kokkos::deep_copy (x_0, x_1);
            Kokkos::deep_copy (p_0, p_1);
            Kokkos::deep_copy (r_0, r_1);
            Kokkos::deep_copy (v_0, v_1);
            // x_0 = x_1; 
            // p_0 = p_1; 
            // r_0 = r_1;  
            // v_0 = v_1; 
    

            rho_0 = rho_1;
            omega_0 = omega_1;

            
        } 
        Kokkos::deep_copy (result, x_0);
        num_iter = count;
        A.apply(result, r_0); // r_0 =  A*x_0
        KokkosBlas::axpby(1.0, b, -1.0, r_0); //r_0 = b - A*x_0
        std::cout << "RESIDUE: " << KokkosBlas::nrm2(r_0) <<  std::endl;
        // if (max_iter == 5757){
        //     csv.write_value(path_error,rel_error);
        // }
        std::cout << "Iter: "<< count << std::endl;

    }

    void matrix_free_CG(
        Operator &A,
        Operator &P,
        const KokkosVector &b,
        KokkosVector &x_0,
        const size_t &N,
        KokkosVector &result,
        Real &num_iter)
    {
        Real TOL =  1.0e-10; //set the tolerance
        Integer max_iter = 500;

        Kokkos::View<Real*> M("M", N );
        Kokkos::View<Real*> x_1("Z", N );
        Kokkos::View<Real*> r_0("Z", N );
        Kokkos::View<Real*> r_1("Z", N );
        Kokkos::View<Real*> z_0("Z", N );
        Kokkos::View<Real*> p_0("Z", N );
        Kokkos::View<Real*> z_1("Z", N );
        Kokkos::View<Real*> p_1("Z", N );
        Kokkos::View<Real*> Ap("Z", N );


        A.apply(x_0, r_0); // r_0 =  A*x_0
        KokkosBlas::axpy(-1.0, r_0, b); // r_0 = b - A*x_0
        Kokkos::deep_copy (r_0, b);

        P.apply(r_0, z_0); // z_0 = M*r_0

        int count = 0;
        Kokkos::deep_copy (p_0, z_0);
        while (count < max_iter){
            
            count++;
            Real alpha_num = KokkosBlas::dot(r_0,z_0); // (r_k)^T * z_k
            A.apply(p_0,Ap); // Ap= A*p_0
            
            Real alpha_den = KokkosBlas::dot(p_0,Ap); // (p_k)^T * A * p_k
            if (alpha_den == 0){
                // terminate the loop
                break;
            }
            Real alpha_new =  alpha_num/alpha_den; // alpha 

            KokkosBlas::scal(x_1,alpha_new,p_0);// alpha_k *  p_k

            KokkosBlas::axpy(1.0, x_0, x_1); // x_new = x_old + alpha_k *  p_k
            // Kokkos::deep_copy (x_1, x_0);

            KokkosBlas::scal(r_1,alpha_new,Ap);// alpha_new * A * p_k

            // KokkosBlas::axpy(-1.0, r_0, r_1); // r_new = r_old - alpha_new * A * p_k
            KokkosBlas::axpby(1.0, r_0, -1.0, r_1);
            // Kokkos::deep_copy (r_1, r_0);

            // VectorWrapper precon(M); 
            P.apply(r_1, z_1);// z_new = preconditioner * r_new 

            Real beta_num = KokkosBlas::dot(z_1,r_1); // (z_new)^T * r_new 
            
            Real beta_den = KokkosBlas::dot(z_0, r_0); // (z_old)^T * r_old
            if (beta_den == 0){
                // terminate the loop
                break;
            }
            
            Real beta_new = beta_num/beta_den; // beta_new 
            KokkosBlas::scal(p_1,beta_new,p_0); // beta_new * p_old 
            KokkosBlas::axpy(1.0, z_1, p_1); // p_new = z_new + beta_new * p_old 

      

            Kokkos::deep_copy (x_0, x_1);
            Kokkos::deep_copy (p_0, p_1);
            Kokkos::deep_copy (r_0, r_1);
            Kokkos::deep_copy (z_0, z_1);

            double eps = KokkosBlas::nrm2(r_0);
            if (eps < TOL){
                // terminate the loop
                break;
            }

            
        }

        Kokkos::deep_copy (result, x_0);
        num_iter = count;
        
    } 


    void preconCG(
    const SparseMatrix &A, 
    const SparseMatrix &M, 
    const KokkosVector &b, 
    std::vector<bool> &node_is_boundary, 
    const Real &dt, 
    KokkosVector &init_guess, 
    KokkosVector &result,
    Real &num_iter) {

        Integer N =  A.numRows();
        // Kokkos::View<Real*> x_0("x_0", N );
        Kokkos::View<Real*> a("diag_A", N );
        Kokkos::View<Real*> Precon("precond", N );

        // ImplicitEulerOperator S(A,M,dt,N);
        MatrixWrapper S(A);
        
        for(Integer i = 0; i < N; ++i) {
            auto row = A.row(i);
            auto n_vals = row.length;
            for(Integer k = 0; k < n_vals; ++k) {
                auto j = row.colidx(k);
                if (i==j){
                    a(i) = 1./row.value(k);
                    // a(i) = row.value(k);
                    // std::cout << a(i) <<  std::endl;
                }
            }
        }

    
        // for(int i=0;i<N;i++){
        //     if (node_is_boundary[i]  == 1){
        //         init_guess(i)=b(i); 
        //     } else {
        //         init_guess(i)=1.0;  
        //     }
        // }

        // display_vector(init_guess, N);


        Integer  max_iter = N;
        VectorWrapper P(a);

        double o1;
        P.apply2(o1);
        // matrix_free_CG(S, P, b, x_0, N, result, num_iter);
        
        BiCGSTAB(S, P, b, init_guess, N, max_iter, o1, result, num_iter);
    }


}

#endif //ST_CG_HPP