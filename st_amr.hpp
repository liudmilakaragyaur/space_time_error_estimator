#ifndef ST_ADAPTIVE_REF
#define ST_ADAPTIVE_REF

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <err.h>

#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include "mars_simplex.hpp" 
#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_static_math.hpp"
#include "mars_mesh.hpp"
#include "mars_bisection.hpp"
#include "mars_longest_edge.hpp"
#include "st_utils.hpp"
#include "sparse_matrix.hpp"
#include "precon_conjugate_grad.hpp"
#include "st_rec_grad.hpp"
#include "st_csv_writer.hpp"
#include <Kokkos_Core.hpp>
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv_spec.hpp"
#include <KokkosSparse_spmv.hpp>
#include<KokkosBlas1_axpby.hpp>
// #include<KokkosBlas1_mult.hpp>
// #include<KokkosBlas1_dot.hpp>
// #include<KokkosBlas1_scal.hpp>
// #include<KokkosBlas1_nrm1.hpp>
#include<KokkosBlas1_nrm2.hpp>
// #include<KokkosSparse_spadd.hpp>

namespace mars{

    template<Integer Dim, Integer ManifoldDim, class F>
    class AdaptiveRef  {
        public: 

        using Element = Simplex<Dim, ManifoldDim>; 
        using MyElement = FE<Dim,ManifoldDim>;
        using SideElement = Simplex<Dim, ManifoldDim-1>; 
        using MassAssembler = MassMatrixAssembler<Dim, ManifoldDim>;
        using StiffAssembler = StiffnessMatrixAssembler<Dim, ManifoldDim>;
        using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
        using KokkosVector = Kokkos::View<Real*>;

        

        bool space_time = true;


        void interpolate(
        const size_t &N_el_old, 
        const size_t &N_el_new,
        const size_t &N_node_old, 
        const size_t &N_node_new,
        const KokkosVector &sol_old,
        const Mesh<Dim, ManifoldDim> &mesh,
        KokkosVector &result){
            
            MyElement e;
            for (int i = 0; i < N_node_old; i++){
                result(i) = sol_old(i);
            }   
        

            for (int i = N_el_old; i < mesh.n_elements(); i++){ 
                
                // Integer id_new = N_el_old + i;
                
                e.parent_to_child(mesh, i, sol_old, result);   

            }


        }

        void solve_new (
        const size_t N, 
        const Real &TOL1, 
        KokkosVector &init_guess,
        F rhs,
        std::string &path,
        std::string &path_error,
        Real &mean_error,
        std::vector<Integer> &elements_to_refine_new,
        Mesh<Dim, ManifoldDim> &mesh,
        KokkosVector &result,
        Real &n_iter_tot,
        Integer side_tag,
        F init_cond,
        bool initial = false,
        bool GR = true
        ){
            
            MassMatrixAssembler<Dim,ManifoldDim> M;
            StiffnessMatrixAssembler<Dim,ManifoldDim> S;

            VTUMeshWriter<Mesh<Dim,ManifoldDim>> w;
            CSVWriter csv;
            
            bool upwind = true;
            SparseMatrix mass_upwind = assemble_mass_stiffness(mesh, M, S, space_time, true, upwind);
            SparseMatrix mass_no_upwind = assemble_mass_stiffness(mesh, M, S, space_time, true, false);

            SparseMatrix stiffness = assemble_mass_stiffness(mesh, M, S, space_time, false, upwind);

            std::vector<bool> node_is_boundary(mesh.n_nodes(), false);
            boundary_nodes(mesh, node_is_boundary, space_time, side_tag);

            Kokkos::View<Real*> st_rhs("st_rhs", N );
            for (int k = 0; k < N; ++k){
                st_rhs(k) = rhs(mesh.point(k));
            }  

            Kokkos::View<Real*> Mb("Mb", N);
            KokkosSparse::spmv("N", 1.0, mass_upwind, st_rhs, 0.0, Mb);

            assign_boundary_cond(mesh, stiffness, Mb, node_is_boundary);
            if (initial){   
                assign_init_cond(mesh, init_cond, Mb);
                // assign_init_cond(mesh, init_cond, Mb_noBC);
            }


            // Kokkos::View<Real*> result("result", N);

            
            preconCG(stiffness, mass_upwind, Mb, node_is_boundary, 0, init_guess, result, n_iter_tot); 
            w.write_vtu(path, mesh, result);

            if (GR){
                rec_grad(mesh, mass_no_upwind, result, node_is_boundary, TOL1, elements_to_refine_new, mean_error, path_error);

            } else {

                res_based(mesh, result, rhs, elements_to_refine_new, mean_error, -1,-1, TOL1, path_error); //6,3 for 3D
            }

            // return result;

        }


    };

    template<Integer Dim, Integer ManifoldDim, class F>
    void adaptive_scheme(
    Mesh<Dim, ManifoldDim> &mesh, 
    const Real &TOL1, 
    const Real &TOL2, 
    const Integer &max_iter, 
    KokkosVector &solution_old,
    std::vector<Integer> &elements_to_refine,
    Integer side_tag,
    F rhs,
    F init_cond,
    bool initial = false,
    bool GR = true) 
    {
        // 3D
        // std::string path_error_cvs = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/error/error_3D.csv";
        // std::string path_dofs_cvs = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/error/dofs_3D.csv";
        // 2D
        std::string path_error_cvs = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/error/error_2D.csv";
        std::string path_error_exact_cvs = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/error/exact_error_2D.csv";
        std::string path_dofs_cvs = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/error/dofs_2D.csv";


        Bisection<Mesh<Dim,ManifoldDim>> b(mesh);
        Quality<Mesh<Dim,ManifoldDim>> q(mesh);
        AdaptiveRef<Dim,ManifoldDim,F> adapt;
        VTUMeshWriter<Mesh<Dim,ManifoldDim>> w;
        CSVWriter csv;

        // std::string path_iter = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/csv2/init_normal/iter.csv";



        size_t N = mesh.n_nodes();
        KokkosVector  interp("interp", N);
        KokkosVector  new_sol("new_sol", N);
        std::vector<Real> number_iterations;
        std::vector<Integer> number_dofs;
        std::vector<Real> L2_error;
        std::vector<Real> exact_error;

        size_t N_el_old = mesh.n_elements();
        size_t N_node_old = mesh.n_nodes();


        int count = 0;
        while (count < max_iter){
            


            count++;
            // 2D 
            std::string path = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/refinements/ref" + std::to_string(count) + ".vtu";
            std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/error/error" + std::to_string(count) + ".vtu";
            std::string path_inter = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/interp_sol/interp" + std::to_string(count) + ".vtu";

            // //3D 
            // std::string path = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/refinements/ref" + std::to_string(count) + ".vtu";
            // std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/error/error" + std::to_string(count) + ".vtu";
            // std::string path_inter = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/interp/interp" + std::to_string(count) + ".vtu";

            b.refine(elements_to_refine);
            b.clear();

            
            size_t new_el = mesh.n_active_elements();
            size_t new_nodes = mesh.n_nodes();  

 
            Kokkos::resize(interp, new_nodes);
            adapt.interpolate(N_el_old, new_el, N_node_old, new_nodes, solution_old, mesh, interp);  

            w.write_vtu(path_inter, mesh, interp);

            mesh.clean_up();
            // mesh.renumber_nodes();
            mesh.update_dual_graph();

            write("/Users/liudmilakaragyaur/code/space_time_error_estimator/mesh.msh", mesh);


            size_t N_node_new = mesh.n_nodes();
            size_t N_el_new = mesh.n_elements();
            
            // Kokkos::resize(new_sol, N_node_new);


            // std::cout << N_el_new << "||" << N_node_new << std::endl;

            Real mean_error;
            Real n_iter_tot;
            std::vector<Integer> elements_to_refine_new;
            KokkosVector  new_sol("new_sol", N_node_new);
            KokkosVector  sol_exact("sol_exact", N_node_new);
            // KokkosVector  zero_init("zero_init", N_node_new);
            // fill_vector(zero_init, N_node_new, 0.0);

            adapt.solve_new(N_node_new, TOL1, interp, rhs,  path, path_error, mean_error, elements_to_refine_new, mesh, new_sol, n_iter_tot, side_tag, init_cond, initial, GR);
            number_iterations.push_back(n_iter_tot);
            std::cout << "Error: " << mean_error  << std::endl;

            // exact solution
            for (int k = 0; k<N_node_new; ++k){
                sol_exact(k) = (mesh.point(k)[0] * mesh.point(k)[0]  - mesh.point(k)[0]) * (mesh.point(k)[1] * mesh.point(k)[1] - mesh.point(k)[1]) * std::exp(-100 * ((mesh.point(k)[0]-mesh.point(k)[1]) * (mesh.point(k)[0]-mesh.point(k)[1]))); 
                //mesh.point(k)[0] * std::cos(3 * M_PI * mesh.point(k)[0]/2) * std::sin(3 * mesh.point(k)[1]);
                
            }   

            Real tot_error = 0;
            compute_exact_error(mesh, sol_exact, new_sol, tot_error);
            std::cout<< "Number of elements  AMR: " << mesh.n_active_elements() <<std::endl;

            std::cout<< "True error: " << tot_error <<std::endl;



            // L2_error.push_back(mean_error);
            // number_dofs.push_back(N_node_new);
            // exact_error.push_back(tot_error);
            int n_dofs = mesh.n_nodes();
            FILE* file = fopen("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_2D/adaptive_ref/output.csv", "a");
            fprintf(file, "%ld, %f, %f\n",n_dofs, mean_error, tot_error);
            fclose (file);

            


            if (mean_error <= TOL2){
                break;
            }

            if (elements_to_refine_new.empty()){
                break;
            }

            Kokkos::resize(solution_old, N_node_new);
            // solution_old = new_sol;
            Kokkos::deep_copy(solution_old, new_sol);
            N_el_old = N_el_new;
            N_node_old = N_node_new;
            elements_to_refine = elements_to_refine_new;



    

            
        }

        // csv.write_value(path_iter,number_iterations);
        // csv.write_value(path_error_cvs, L2_error);
        // csv.write_value(path_error_cvs, exact_error);
        // csv.write_value(path_dofs_cvs, number_dofs);

        

    }


}

#endif //ST_ADAPTIVE_REF