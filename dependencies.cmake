# find dependencies
find_package(Trilinos)

if(TRILINOS_FOUND)
    foreach(LIB ${Trilinos_LIBRARY_DIRS})
        list(APPEND UTOPIA_LIBRARIES -L${LIB})
    endforeach(LIB)

    list(APPEND UTOPIA_INCLUDES ${Trilinos_INCLUDE_DIRS})
    # list(APPEND UTOPIA_LIBRARIES ${Trilinos_LIBRARY_DIRS})
    list(APPEND Trilinos_all_libs ${Trilinos_TPL_LIBRARIES})
    list(APPEND Trilinos_all_libs ${Trilinos_LIBRARIES})
    list(REMOVE_DUPLICATES Trilinos_all_libs)
    foreach(LIB ${Trilinos_all_libs})
        if(EXISTS ${LIB})
            list(APPEND UTOPIA_LIBRARIES ${LIB})
        else()
            list(APPEND UTOPIA_LIBRARIES -l${LIB})
        endif()
    endforeach()
    list(APPEND Trilinos_TPL_INCLUDE_DIRS ${Trilinos_additional_headers})
    list(APPEND UTOPIA_INCLUDES ${Trilinos_TPL_INCLUDE_DIRS})
    #####
    message(STATUS "Trilinos_additional_headers ${Trilinos_additional_headers}")
    message(STATUS "Trilinos_TPL_INCLUDE_DIRS ${Trilinos_TPL_INCLUDE_DIRS}")

    list(FIND Trilinos_PACKAGE_LIST "Amesos2" AMESOS2_FOUND)
    if (NOT AMESOS2_FOUND EQUAL -1)
      set(WITH_TRILINOS_AMESOS2 TRUE PARENT_SCOPE)
    endif()
    list(FIND Trilinos_PACKAGE_LIST "Belos" BELOS_FOUND)
    if (NOT BELOS_FOUND EQUAL -1)
      set(WITH_TRILINOS_BELOS TRUE PARENT_SCOPE)
    endif()
    list(FIND Trilinos_PACKAGE_LIST "Ifpack2" IFPACK2_FOUND)
    if (NOT IFPACK2_FOUND EQUAL -1)
      set(WITH_TRILINOS_IFPACK2 TRUE PARENT_SCOPE)
    endif()
    list(FIND Trilinos_PACKAGE_LIST "MueLu" MUELUE_FOUND)
    if (NOT MUELUE_FOUND EQUAL -1)
      set(WITH_TRILINOS_MUELU TRUE PARENT_SCOPE)
    endif()

    # set(CMAKE_C_COMPILER ${TRILINOS_COMPILER})

    if(NOT MPI_CXX_COMPILER)
        set(MPI_CXX_COMPILER $ENV{MPI_CXX_COMPILER})
        message(STATUS "compiler ${MPI_CXX_COMPILER}")
    endif()

    if(MPI_CXX_COMPILER)
        set(CMAKE_CXX_COMPILER ${MPI_CXX_COMPILER})
        set(CMAKE_CXX_COMPILER_DEBUG ${MPI_CXX_COMPILER})
    else()
         execute_process(COMMAND mpicxx -v RESULT_VARIABLE MPICXX_FAILED)

        if(MPICXX_FAILED)
            message(STATUS "Using CMAKE compiler, you can define MPI_CXX_COMPILER=<alias_or_path_to_your_compiler>")
        else()
            message(STATUS "-----------------------------------------------")
            message(STATUS "\n[MPI] using mpicxx for compiling c++ files.\nIf you want to use your own compiler define MPI_CXX_COMPILER=<alias_or_path_to_your_compiler>")
            message(STATUS "-----------------------------------------------")
            set(CMAKE_CXX_COMPILER mpicxx)
            set(CMAKE_CXX_COMPILER_DEBUG mpicxx)
        endif()
    endif()

    set(WITH_TRILINOS TRUE)

    # find_package(TpetraExt)
    # if(TRILINOS_TPETRAEXT_FOUND)
    #     set(WITH_TRILINOS_TPETRAEXT TRUE PARENT_SCOPE)
    # endif()
else()
    message(WARNING "[Warning] Trilinos not found")
    set(WITH_TRILINOS FALSE)
endif()

# set-up module
if(TRILINOS_FOUND)

    # foreach(MODULE ${TRILINOS_MODULES})
    #     target_include_directories(sp_exec PUBLIC ${MODULE})
    # endforeach(MODULE)

    # target_include_directories(sp_exec PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}  ${CMAKE_CURRENT_SOURCE_DIR}/..)
    # target_include_directories(sp_exec PUBLIC ${Trilinos_INCLUDE_DIRS} ${Trilinos_TPL_INCLUDE_DIRS})

    # target_link_libraries(sp_exec ${Trilinos_LIBRARIES})
    # target_link_libraries(sp_exec ${Trilinos_TPL_LIBRARIES})
    # target_link_libraries(sp_exec "-L${Trilinos_LIBRARY_DIRS}")


    include_directories(${CMAKE_CURRENT_SOURCE_DIR}  ${CMAKE_CURRENT_SOURCE_DIR}/..)
    include_directories(${Trilinos_INCLUDE_DIRS} ${Trilinos_TPL_INCLUDE_DIRS})

    link_libraries(${Trilinos_LIBRARIES})
    link_libraries(${Trilinos_TPL_LIBRARIES})
    link_libraries("-L${Trilinos_LIBRARY_DIRS}")


    add_definitions(${TRILINOS_DEFINITIONS})
endif()
