#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <math.h>   
#include <err.h>
#include <stdio.h>

#include "mars_simplex.hpp"
#include "mars_lagrange_element.hpp"
#include "mars_mesh.hpp"
#include "mars_bisection.hpp"
#include "mars_vtk_writer.hpp"
#include "mars_quality.hpp"
#include "mars_utils.hpp"
#include "mars_mesh_partition.hpp"
#include "mars_partitioned_bisection.hpp"
#include "mars_benchmark.hpp"
#include "mars_lepp_benchmark.hpp"
#include "mars_mesh_generation.hpp"
#include "mars_test.hpp"
#include "mars_ranked_edge.hpp"
#include "mars_oldest_edge.hpp"
#include "mars_longest_edge.hpp"
#include "mars_memory.hpp"
#include "mars_mesh_reader.hpp"
#include "mars_mesh_writer.hpp"
#include "mars_matrix.hpp"
#include "st_element.hpp"
#include "st_quadrature.hpp"
#include "matrix_free_operator.hpp"
#include "implicit_euler.hpp"
#include "precon_conjugate_grad.hpp"
#include "sparse_matrix.hpp"
#include "st_utils.hpp"
#include "st_rec_grad.hpp"
#include "st_amr.hpp"
#include "st_res_based.hpp"
#include "Kokkos_Core.hpp"
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv.hpp"
#include "KokkosBlas1_scal.hpp"
#include "mars_mesh_generation.hpp"
#include "vtu_writer.hpp"


namespace mars {

    inline Real rhs_heat_3D(const Vector<Real, 3> &x){ 
        // return  (2*M_PI*M_PI-1) * std::exp(-x[2]) * std::sin(M_PI * x[0]) * std::sin(M_PI * x[1]);

        // return  (std::exp(- 100.0*(x[2] - x[0])*(x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[0]*(x[0]) + x[0])*(- x[1]*(x[1]) + x[1])*(2*x[2] - 1) - std::exp(- 100.0*(x[2] - x[0])*(x[2] - x[0]) - 100*(x[2] - x[1]) *(x[2] - x[1]))*(- x[2]*(x[2]) + x[2])*(- x[0]*(x[0]) + x[0])*(- x[1]*(x[1]) + x[1])*(200.0*x[0] - 400*x[2] + 200.0*x[1]) ) - 
        // ((2.0*std::exp(- 100.0*(x[2] - x[0]) * (x[2] - x[0])  - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2]*(x[2]) + x[2])*(- x[1]*(x[1]) + x[1]) + 200.0*std::exp(- 100.0*(x[2] - x[0]) * (x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2]* (x[2]) + x[2])*(- x[0] * (x[0]) + x[0])*(- x[1] * (x[1]) + x[1]) + 2*std::exp(- 100.0*(x[2] - x[0]) * (x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2]*(x[2]) + x[2])*(200.0*x[2] - 200.0*x[0])*(- x[1]*(x[1]) + x[1])*(2*x[0] - 1) - std::exp(- 100.0*(x[2] - x[0])*(x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2] * (x[2]) + x[2])*(200.0*x[2] - 200.0*x[0]) * (200.0* x[2] - 200.0*x[0]) *  (-x[0] * (x[0]) + x[0])*(- x[1] * (x[1]) + x[1])) + 
        // (2.0*std::exp(- 100.0*(x[2] - x[0]) * (x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2] * (x[2]) + x[2])*(- x[0]*(x[0]) + x[0]) + 200.0*std::exp(- 100.0*(x[2] - x[0]) * (x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2]* (x[2]) + x[2])*(- x[0] * (x[0]) + x[0])*(- x[1] * (x[1]) + x[1]) + 2*std::exp(- 100.0*(x[2] - x[0]) * (x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2]*(x[2]) + x[2])*(200.0*x[2] - 200.0*x[1])*(- x[0]*(x[0]) + x[0])*(2*x[1] - 1) - std::exp(- 100.0*(x[2] - x[0])*(x[2] - x[0]) - 100.0*(x[2] - x[1]) * (x[2] - x[1]))*(- x[2] * (x[2]) + x[2])*(200.0*x[2] - 200.0*x[1]) * (200.0 *x[2] - 200.0*x[1]) * (- x[0] * (x[0]) + x[0])*(- x[1] * (x[1]) + x[1])));

        // return (std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[1]*x[1] + x[1])*(2*x[2] - 1)*(- 100.0*x[0]*x[0] + 100.0*x[0]) + std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- x[1]*x[1] + x[1])*(200.0*x[2] - 50.0)*(- 100.0*x[0]*x[0] + 100.0*x[0])) - 
        // ( (200.0*std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- x[1]*x[1] + x[1]) + 200.0*std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- x[1]*x[1] + x[1])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- x[1]*x[1] + x[1])*(200.0*x[0] - 50.0)*(200.0*x[0] - 100.0) - std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- x[1]*x[1] + x[1])*(200.0*x[0] - 50.0)*(200.0*x[0] - 50.0)*(- 100.0*x[0]*x[0] + 100.0*x[0])) +
        // (2*std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) + 200.0*std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- x[1]*x[1] + x[1])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(2*x[1] - 1)*(200.0*x[1] - 50.0)*(- 100.0*x[0]*x[0] + 100.0*x[0]) - std::exp(- 100.0*(x[2] - 1./4)*(x[2] - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[2]*x[2] + x[2])*(- x[1]*x[1] + x[1])*(200.0*x[1] - 50.0)*(200.0*x[1] - 50.0) *(- 100.0*x[0]*x[0] + 100.0*x[0]) ) );


        // return  std::exp(x[2])*sin(M_PI*x[0])*sin(M_PI*x[1]) - ((-M_PI*M_PI*std::exp(x[2])*sin(M_PI*x[0])*sin(M_PI*x[1])) + (-M_PI*M_PI*std::exp(x[2])*sin(M_PI*x[0])*sin(M_PI*x[1])) );

        // return M_PI * std::sin(M_PI *x[0]) * std::sin(M_PI *x[1]) * ( std::cos(M_PI * x[2]) + 2.0* M_PI * std::sin(M_PI * x[2]));
    
        // return (- x[2]*x[2]*x[0]*x[0]*x[1]*x[1]*(x[0] - 1.0)*(x[1] - 1.0) - 2*x[2]*x[0]*x[0]*x[1]*x[1]*(x[2] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0)) - 
        // (( - 4.0*x[2]*x[2]*x[0]*x[1]*x[1]*(x[2] - 1.0)*(x[1] - 1.0) - 2*x[2]*x[2]*x[1]*x[1]*(x[2] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0)) +
        // (- 4.0*x[2]*x[2]*x[0]*x[0]*x[1]*(x[2] - 1.0)*(x[0] - 1.0) - 2*x[2]*x[2]*x[0]*x[0]*(x[2] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0) ));

        return (-std::exp(- 200*(x[0] - std::cos((2*M_PI*x[2])/5)/2)*(x[0] - std::cos((2*M_PI*x[2])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[2])/5)/2)*(x[1] - std::sin((2*M_PI*x[2])/5)/2))*(80*M_PI*std::sin((2*M_PI*x[2])/5)*(x[0] - std::cos((2*M_PI*x[2])/5)/2) - 80*M_PI*std::cos((2*M_PI*x[2])/5)*(x[1] - std::sin((2*M_PI*x[2])/5)/2))) - 
        ( (std::exp(- 200*(x[0] - std::cos((2*M_PI*x[2])/5)/2)*(x[0] - std::cos((2*M_PI*x[2])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[2])/5)/2)*(x[1] - std::sin((2*M_PI*x[2])/5)/2))*(400*x[0] - 200*std::cos((2*M_PI*x[2])/5))*(400*x[0] - 200*std::cos((2*M_PI*x[2])/5)) - 400*std::exp(- 200*(x[0] - std::cos((2*M_PI*x[2])/5)/2)*(x[0] - std::cos((2*M_PI*x[2])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[2])/5)/2)*(x[1] - std::sin((2*M_PI*x[2])/5)/2))) + 
        (std::exp(- 200*(x[0] - std::cos((2*M_PI*x[2])/5)/2)*(x[0] - std::cos((2*M_PI*x[2])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[2])/5)/2)*(x[1] - std::sin((2*M_PI*x[2])/5)/2))*(400*x[1] - 200*std::sin((2*M_PI*x[2])/5))*(400*x[1] - 200*std::sin((2*M_PI*x[2])/5)) - 400*std::exp(- 200*(x[0] - std::cos((2*M_PI*x[2])/5)/2)*(x[0] - std::cos((2*M_PI*x[2])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[2])/5)/2)*(x[1] - std::sin((2*M_PI*x[2])/5)/2)) ));




    }

    inline Real init_heat_3D(const Vector<Real, 3> &x){ 
        // return  std::sin(M_PI * x[0]) * std::sin(M_PI * x[1]);
        
        //  return (x[0]* x[0] - x[0]) * (x[1]* x[1] - x[1]) * (0.0* 0.0 - 0.0) * std::exp(-100.0 * ((x[0] - 0.0) * (x[0] - 0.0) + (x[1] - 0.0) * (x[1] - 0.0)));
        
        // return (std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- x[1]*x[1] + x[1])*(2*0.0 - 1)*(- 100.0*x[0]*x[0] + 100.0*x[0]) + std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(200.0*0.0 - 50.0)*(- 100.0*x[0]*x[0] + 100.0*x[0])) - 
        // ( (200.0*std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1]) + 200.0*std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(200.0*x[0] - 50.0)*(200.0*x[0] - 100.0) - std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(200.0*x[0] - 50.0)*(200.0*x[0] - 50.0)*(- 100.0*x[0]*x[0] + 100.0*x[0])) +
        // (2*std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- 100.0*x[0]*x[0] + 100.0*x[0]) + 200.0*std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(2*x[1] - 1)*(200.0*x[1] - 50.0)*(- 100.0*x[0]*x[0] + 100.0*x[0]) - std::exp(- 100.0*(0.0 - 1./4)*(0.0 - 1./4) - 100.0*(x[0] - 1./4)*(x[0] - 1./4) - 100.0*(x[1] - 1./4)*(x[1] - 1./4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(200.0*x[1] - 50.0)*(200.0*x[1] - 50.0) *(- 100.0*x[0]*x[0] + 100.0*x[0]) ) );


        // return std::exp(0.0)*sin(M_PI*x[0])*sin(M_PI*x[1]) - ((-M_PI*M_PI*std::exp(0.0)*sin(M_PI*x[0])*sin(M_PI*x[1])) + (-M_PI*M_PI*std::exp(0.0)*sin(M_PI*x[0])*sin(M_PI*x[1])) );
        // return x[0] * 0;

        // return (1-x[0]) * x[0] * x[0] * (1-x[1]) * x[1] * x[1] * (1-0.0) * 0.0 * 0.0; 


        Real A = 1; 
        Real sigma = 5e-02;
        Real r = 0.5; 
        Real p = 5;

        Real gamma_1 = x[0] - r * (std::cos(2*M_PI *0.0 / p));
        Real gamma_2 = x[1] - r * (std::sin(2*M_PI *0.0 / p));
        
        Real num = gamma_1 * gamma_1 + gamma_2 * gamma_2;

        return A * std::exp(-num / (2.0*sigma*sigma));



    } 

    inline Real exact_heat_3D(const Vector<Real, 3> &x){ 
        // return  std::exp(-x[2]) * std::sin(M_PI * x[0]) * std::sin(M_PI * x[1]);
        // return (x[0]* x[0] - x[0]) * (x[1]* x[1] - x[1]) * (x[2]* x[2] - x[2]) * std::exp(-100.0 * ((x[0] - x[2]) * (x[0] - x[2]) + (x[1] - x[2]) * (x[1] - x[2])));
        // return 100.0* (x[0]* x[0] - x[0]) * (x[1]* x[1] - x[1]) * (x[2]* x[2] - x[2]) * std::exp(-100.0 * ((x[0] - 0.25) * (x[0] - 0.25) + (x[1] - 0.25) * (x[1] - 0.25) + (x[2] - 0.25) * (x[2] - 0.25)));

        // return std::sin(M_PI * x[0]) * std::sin(M_PI * x[1])  * std::exp(x[2]);

        // return  std::sin(M_PI*x[0]) *  std::sin(M_PI*x[1]) * std::sin(M_PI*x[2]);

        // return (1-x[0]) * x[0] * x[0] * (1-x[1]) * x[1] * x[1] * (1-x[2]) * x[2] * x[2]; 

            
        Real A = 1; 
        Real sigma = 5e-02;
        Real r = 0.5; 
        Real p = 5;

        Real gamma_1 = x[0] - r * (std::cos(2*M_PI *x[2] / p));
        Real gamma_2 = x[1] - r * (std::sin(2*M_PI *x[2] / p));
        
        Real num = gamma_1 * gamma_1 + gamma_2 * gamma_2;

        return A * std::exp(-num / (2.0*sigma*sigma));
    } 

}


void test_space_time_3D(int &n_lev)
{

    using namespace mars;
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    
    CSVWriter csv;
    VTUMeshWriter<Mesh3> w;
    Mesh3 mesh;
    // generate_cube<3, 3>(mesh,2, 2,2);
    // generate_cube<3, 3>(mesh,10,10,10);
    // mesh.build_dual_graph();

    read("/Users/liudmilakaragyaur/code/test_cube.msh", mesh, true);
    mark_boundary(mesh);   

    Bisection<Mesh3> b(mesh);
    b.uniform_refine(n_lev); b.clear();
    mesh.clean_up();
    mesh.update_dual_graph();
    print_boundary_info(mesh, true);



    // mesh.describe_dual_graph(std::cout);

    // mesh.reorder_nodes();

    // mark_boundary(mesh);   
    // mesh.build_dual_graph(); 
    // mesh.describe(std::cout);

    write("/Users/liudmilakaragyaur/code/space_time_error_estimator/mesh_3D.msh", mesh);



    bool space_time = true;
    MassMatrixAssembler<3,3> M;
    StiffnessMatrixAssembler<3,3> S;
    bool upwind = true;
    SparseMatrix mass_upwind = assemble_mass_stiffness(mesh, M, S, space_time, true, upwind);
    SparseMatrix mass_no_upwind = assemble_mass_stiffness(mesh, M, S, space_time, true, false);
    SparseMatrix stiffness = assemble_mass_stiffness(mesh,M, S, space_time, false, upwind);

    // display_sparse(mesh,mass);
    // display_sparse(mesh,stiffness);

    std::string path_matrix = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/stiff.csv";
    std::string path_matrix_mass = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/mass.csv";

    
    Integer n_nodes = mesh.n_nodes();
    // csv.write_sparse(path_matrix, n_nodes, stiffness);
    Integer side_tag = 3; //-1?
    std::vector<bool> node_is_boundary(mesh.n_nodes(), false);
    boundary_nodes(mesh, node_is_boundary, space_time, side_tag); 

    const size_t N = mesh.n_nodes();

    Kokkos::View<Real*> st_rhs("st_rhs", N );
    Kokkos::View<Real*> init_guess("init_guess", N );
    fill_vector(init_guess, N, 0.5);
    Kokkos::View<Real*> result_st("result_st", N );
    Kokkos::View<Real*> exact_st("exact_st", N );
    for (int k = 0; k<N; ++k){
        st_rhs(k) = rhs_heat_3D(mesh.point(k));
        // 1.0;
        //rhs_heat_3D(mesh.point(k));
    }   

    
    Kokkos::View<Real*> Mb("Mb", N );

    KokkosSparse::spmv("N", 1.0, mass_upwind, st_rhs, 0.0, Mb);

    assign_boundary_cond(mesh, stiffness, Mb, node_is_boundary); 
    assign_init_cond(mesh, init_heat_3D, Mb);

    
    // csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/rhs.csv",N, Mb);
    // csv.write_sparse("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/uniform_matrices/matrix" + std::to_string(n_lev) + ".csv", n_nodes, stiffness);
    // csv.write_sparse(path_matrix_mass, n_nodes, mass);

    // display_vector(Mb,N);
    Real num_iter;
    preconCG(stiffness, mass_upwind, Mb, node_is_boundary, 0, init_guess, result_st, num_iter); 
    std::string path3 = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/result_3D.vtu";
    bool coord = true;
    w.write_vtu(path3, mesh, result_st);
    csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/res_mine.csv",N, result_st);

    for (int k = 0; k<N; ++k){
        exact_st(k) = exact_heat_3D(mesh.point(k));
        //exact_3D(mesh.point(k));
        //exact_heat_3D(mesh.point(k));

    } 

    

    csv.write_sparse_vector("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/res_ex.csv",N, exact_st);

    std::string path4 = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/exact_3D.vtu";
    w.write_vtu(path4, mesh, exact_st);
    // display_vector(result_st,N);




    /////////////////////////////////////////////////////
    std::vector<Integer> elements_to_refine;
    Real TOL  = 1e-2;
    KokkosVector E("E", N );
    Real mean_error_RG;
    std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/error.vtu";
    rec_grad(mesh, mass_no_upwind, result_st,  node_is_boundary, TOL, elements_to_refine, mean_error_RG, path_error);
    Real TOL1 = 1e-8;
    Real TOL2 = 1e-8;
    Integer max_it = 100;

    

    Real mean_error_RB;
    std::string path_res_bas = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/error_res_based.vtu";
    std::vector<Integer> elements_to_refine_RB;
    res_based(mesh, result_st, rhs_heat_3D, elements_to_refine_RB, mean_error_RB, 6,3, TOL,path_res_bas);

    adaptive_scheme( mesh, TOL1, TOL2, max_it, result_st, elements_to_refine, side_tag, rhs_heat_3D, init_heat_3D, exact_heat_3D, true, true);

    // adaptive_scheme(mesh, TOL1, TOL2, max_it, result_st, elements_to_refine_RB, side_tag, rhs_heat_3D, init_heat_3D, exact_heat_3D, true, false);

    
    // Real tot_error = 0;
    // compute_exact_error(mesh, exact_st, result_st,tot_error);
    // std::cout<< "True error: " << tot_error << std::endl;

    // FILE* file = fopen("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/uniform_ref/output.csv", "a");
    // fprintf(file, "%ld, %e, %e, %e\n",mesh.n_nodes(), mean_error_RB, mean_error_RG,tot_error);
    // fclose (file);  


}





int main(int argc, char *argv[])
{
	using namespace mars;

    Kokkos::initialize(argc,argv);

    char *end_ptr = argv[1];
    int level = strtol(argv[1], &end_ptr, 10);

    test_space_time_3D(level); 


    Kokkos::finalize();

    return 0; 
}