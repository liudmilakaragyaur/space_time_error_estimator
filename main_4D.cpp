#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>
#include <math.h>   
#include <err.h>
#include <stdio.h>

#include "mars_simplex.hpp"
#include "mars_lagrange_element.hpp"
#include "mars_mesh.hpp"
#include "mars_bisection.hpp"
#include "mars_vtk_writer.hpp"
#include "mars_quality.hpp"
#include "mars_utils.hpp"
#include "mars_mesh_partition.hpp"
#include "mars_partitioned_bisection.hpp"
#include "mars_benchmark.hpp"
#include "mars_lepp_benchmark.hpp"
#include "mars_mesh_generation.hpp"
#include "mars_test.hpp"
#include "mars_ranked_edge.hpp"
#include "mars_oldest_edge.hpp"
#include "mars_longest_edge.hpp"
#include "mars_memory.hpp"
#include "mars_mesh_reader.hpp"
#include "mars_mesh_writer.hpp"
#include "mars_matrix.hpp"
#include "st_element.hpp"
#include "st_quadrature.hpp"
#include "matrix_free_operator.hpp"
#include "implicit_euler.hpp"
#include "precon_conjugate_grad.hpp"
#include "sparse_matrix.hpp"
#include "st_utils.hpp"
#include "st_rec_grad.hpp"
#include "st_amr.hpp"
#include "st_res_based.hpp"
#include "Kokkos_Core.hpp"
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv.hpp"
#include "KokkosBlas1_scal.hpp"
#include "mars_mesh_generation.hpp"
#include "vtu_writer.hpp"


namespace mars {

    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;
    using KokkosVector = Kokkos::View<Real*>;

    inline Real rhs_heat_4D(const Vector<Real,4> &x){ 
        // return (std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[0] - 600.0*x[3] + 200.0*x[1] + 200.0*x[2]) - std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*x[3] - 1)) - 
        // ((std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[1])*(200.0*x[3] - 200.0*x[1])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 200.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0])- 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[1])*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2])*(2*x[1] - 1.0)) + 
        // (std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[0])*(200.0*x[3] - 200.0*x[0])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 200.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0])- 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*x[0] - 1.0)) + 
        // (std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[2])*(200.0*x[3] - 200.0*x[2])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 200.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0])- 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1]) * (x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[2])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(2*x[2] - 1.0)));
        
        //////////////////////////////////////////////
        // return (std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0])*(200*x[0] - 600*x[3] + 200*x[1] + 200*x[2]) - std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*x[3] - 1)*(- 100.0*x[0]*x[0] + 100.0*x[0])) - 
        // ((std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[0])*(200.0*x[3] - 200.0*x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[0] - 100.0)) + 
        // (std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[1])*(200.0*x[3] - 200.0*x[1])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*    std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[1])*(- x[2]*x[2] + x[2])*(2.0*x[1] - 1.0)*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0])) +
        // (std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[2])*(200.0*x[3] - 200.0*x[2])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*    std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(200.0*x[3] - 200.0*x[2])*(- x[1]*x[1] + x[1])*(2.0*x[2] - 1.0)*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(x[3] - x[0])*(x[3] - x[0]) - 100.0*(x[3] - x[1])*(x[3] - x[1]) - 100.0*(x[3] - x[2])*(x[3] - x[2]))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- 100.0*x[0]*x[0] + 100.0*x[0])));

        //////////////////////////////////////////////

        // return (std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(2.0*x[3] - 2.0) * (2.0*x[3] - 2.0)) - 
        // ((-M_PI*M_PI*std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(x[3] - 1.0) * (x[3] - 1.0)) + 
        // (-M_PI*M_PI*std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(x[3] - 1.0) * (x[3] - 1.0)) + 
        // (-M_PI*M_PI*std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(x[3] - 1.0) * (x[3] - 1.0) ) );

        // return (-std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*x[3] - 1) - std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[3] - 50.0)) -
        // ( (2.0*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*x[0] - 1)*(200.0*x[0] - 50.0) - 200.0*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) + std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[0] - 50.0)*(200.0*x[0] - 50.0)) +
        // (2.0*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2])*(2*x[1] - 1)*(200.0*x[1] - 50.0) - 200.0*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2]) + std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[1] - 50.0)*(200.0*x[1] - 50.0)) +
        // (2.0*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(2*x[2] - 1)*(200.0*x[2] - 50.0) - 200.0*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1]) + std::exp(- 100.0*(x[3] - 1/4)*(x[3] - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[3]*x[3] + x[3])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[2] - 50.0)*(200.0*x[2] - 50.0)));

    
        // return -(2.0*x[3]*x[1]*x[2]*(x[3] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0) + 2.0*x[3]*x[0]*x[2]*(x[3] - 1.0)*(x[0] - 1.0)*(x[2] - 1.0) + 2.0*x[3]*x[0]*x[1]*(x[3] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0) + 2.0*x[0]*x[1]*x[2]*(x[0] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0));  // Poisson test 

        // return (x[3]*x[3]*x[0]*x[0]*x[1]*x[1]*x[2]*x[2]*(x[0] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0) + 2*x[3]*x[0]*x[0]*x[1]*x[1]*x[2]*x[2]*(x[3] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0)) - 
        // ((2*x[3]*x[3]*x[1]*x[1]*x[2]*x[2]*(x[3] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0) + 4*x[3]*x[3]*x[0]*x[1]*x[1]*x[2]*x[2]*(x[3] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0)) + 
        // (2*x[3]*x[3]*x[0]*x[0]*x[2]*x[2]*(x[3] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0) + 4*x[3]*x[3]*x[0]*x[0]*x[1]*x[2]*x[2]*(x[3] - 1.0)*(x[0] - 1.0)*(x[2] - 1.0)) + 
        // (2*x[3]*x[3]*x[0]*x[0]*x[1]*x[1]*(x[3] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0)*(x[2] - 1.0) + 4*x[3]*x[3]*x[0]*x[0]*x[1]*x[1]*x[2]*(x[3] - 1.0)*(x[0] - 1.0)*(x[1] - 1.0) ));

        return (-std::exp(- 200*(x[0] - std::cos((2*M_PI*x[3])/5)/2)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[2] - std::cos((2*M_PI*x[3])/5)/2)*(x[2] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[3])/5)/2)*(x[1] - std::sin((2*M_PI*x[3])/5)/2))*(80*M_PI*std::sin((2*M_PI*x[3])/5)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 80*M_PI*std::cos((2*M_PI*x[3])/5)*(x[1] - std::sin((2*M_PI*x[3])/5)/2) + 80*M_PI*std::sin((2*M_PI*x[3])/5)*(x[2] - std::cos((2*M_PI*x[3])/5)/2)) ) - 
        (( std::exp(- 200*(x[0] - std::cos((2*M_PI*x[3])/5)/2)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[2] - std::cos((2*M_PI*x[3])/5)/2)*(x[2] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[3])/5)/2)*(x[1] - std::sin((2*M_PI*x[3])/5)/2))*(400*x[0] - 200*std::cos((2*M_PI*x[3])/5))*(400*x[0] - 200*std::cos((2*M_PI*x[3])/5)) - 400*std::exp(- 200*(x[0] - std::cos((2*M_PI*x[3])/5)/2)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[2] - std::cos((2*M_PI*x[3])/5)/2)*(x[2] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[3])/5)/2)*(x[1] - std::sin((2*M_PI*x[3])/5)/2))) +
        (std::exp(- 200*(x[0] - std::cos((2*M_PI*x[3])/5)/2)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[2] - std::cos((2*M_PI*x[3])/5)/2)*(x[2] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[3])/5)/2)*(x[1] - std::sin((2*M_PI*x[3])/5)/2))*(400*x[1] - 200*std::sin((2*M_PI*x[3])/5))*(400*x[1] - 200*std::sin((2*M_PI*x[3])/5)) - 400*std::exp(- 200*(x[0] - std::cos((2*M_PI*x[3])/5)/2)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[2] - std::cos((2*M_PI*x[3])/5)/2)*(x[2] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[3])/5)/2)*(x[1] - std::sin((2*M_PI*x[3])/5)/2))) +
        (std::exp(- 200*(x[0] - std::cos((2*M_PI*x[3])/5)/2)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[2] - std::cos((2*M_PI*x[3])/5)/2)*(x[2] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[3])/5)/2)*(x[1] - std::sin((2*M_PI*x[3])/5)/2))*(400*x[2] - 200*std::cos((2*M_PI*x[3])/5))*(400*x[2] - 200*std::cos((2*M_PI*x[3])/5)) - 400*std::exp(- 200*(x[0] - std::cos((2*M_PI*x[3])/5)/2)*(x[0] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[2] - std::cos((2*M_PI*x[3])/5)/2)*(x[2] - std::cos((2*M_PI*x[3])/5)/2) - 200*(x[1] - std::sin((2*M_PI*x[3])/5)/2)*(x[1] - std::sin((2*M_PI*x[3])/5)/2))));


    }



    inline Real init_heat_4D(const Vector<Real, 4> &x){ 

        // return (std::exp(- 100*(0.0 - x[0])*(0.0 - x[0]) - 100*(0.0 - x[1])*(0.0 - x[1]) - 100*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200*x[0] - 600*0.0 + 200*x[1] + 200*x[2]) - std::exp(- 100*(0.0 - x[0])*(0.0 - x[0]) - 100*(0.0 - x[1])*(0.0 - x[1]) - 100*(0.0 - x[2])*(0.0 - x[2]))*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*0.0 - 1)) - 
        // ((std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[1])*(200.0*0.0 - 200.0*x[1])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 200.0*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0])- 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[1])*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2])*(2*x[1] - 1)) + 
        // (std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[0])*(200.0*0.0 - 200.0*x[0])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 200.0*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0])- 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*x[0] - 1)) + 
        // (std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[2])*(200.0*0.0 - 200.0*x[2])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 200.0*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0])- 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1]) * (0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[2])*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(2*x[2] - 1)));

        // //////////////////////////////////////////////
        // return (std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0])*(200*x[0] - 600*0.0 + 200*x[1] + 200*x[2]) - std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*0.0 - 1)*(- 100.0*x[0]*x[0] + 100.0*x[0])) - 
        // ((std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[0])*(200.0*0.0 - 200.0*x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[0] - 100.0)) + 
        // (std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[1])*(200.0*0.0 - 200.0*x[1])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[1])*(- x[2]*x[2] + x[2])*(2*x[1] - 1)*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0])) +
        // (std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[2])*(200.0*0.0 - 200.0*x[2])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 200.0*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(200.0*0.0 - 200.0*x[2])*(- x[1]*x[1] + x[1])*(2*x[2] - 1)*(- 100.0*x[0]*x[0] + 100.0*x[0]) - 2*std::exp(- 100.0*(0.0 - x[0])*(0.0 - x[0]) - 100.0*(0.0 - x[1])*(0.0 - x[1]) - 100.0*(0.0 - x[2])*(0.0 - x[2]))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- 100.0*x[0]*x[0] + 100.0*x[0])));
        //////////////////////////////////////////////
        // return (std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(2.0*0.0 - 2.0) * (2.0*0.0 - 2.0)) - 
        // ( (-M_PI*M_PI*std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(0.0 - 1.0) *  (0.0 - 1.0)) + 
        // (-M_PI*M_PI*std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(0.0 - 1.0) * (0.0 - 1.0)) + 
        // (-M_PI*M_PI*std::sin(M_PI*x[0])*std::sin(M_PI*x[1])*std::sin(M_PI*x[2])*(0.0 - 1.0) * (0.0 - 1.0) ) ); 

        // return (-std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*0.0 - 1) - std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*0.0 - 50.0)) -
        // ( (2.0*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(2*x[0] - 1)*(200.0*x[0] - 50.0) - 200.0*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) + std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[0] - 50.0)*(200.0*x[0] - 50.0)) +
        // (2.0*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2])*(2*x[1] - 1)*(200.0*x[1] - 50.0) - 200.0*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[2]*x[2] + x[2]) + std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[1] - 50.0)*(200.0*x[1] - 50.0)) +
        // (2.0*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(2*x[2] - 1)*(200.0*x[2] - 50.0) - 200.0*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2]) - 2*std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1]) + std::exp(- 100.0*(0.0 - 1/4)*(0.0 - 1/4) - 100.0*(x[0] - 1/4)*(x[0] - 1/4) - 100.0*(x[1] - 1/4)*(x[1] - 1/4) - 100.0*(x[2] - 1/4)*(x[2] - 1/4))*(- 0.0*0.0 + 0.0)*(- x[0]*x[0] + x[0])*(- x[1]*x[1] + x[1])*(- x[2]*x[2] + x[2])*(200.0*x[2] - 50.0)*(200.0*x[2] - 50.0)));


        // return (1-x[0]) * x[0] * x[0] * (1-x[1]) * x[1] * x[1] * (1-x[2]) * x[2] * x[2] * (1-0.0) * 0.0 * 0.0; 

        Real A = 1; 
        Real sigma = 5e-02;
        Real r = 0.5; 
        Real p = 5;

        Real gamma_1 = x[0] - r * (std::cos(2*M_PI * 0.0 / p));
        Real gamma_2 = x[1] - r * (std::sin(2*M_PI * 0.0 / p));
        Real gamma_3 = x[2] - r * (std::cos(2*M_PI * 0.0 / p));
        
        Real num = gamma_1 * gamma_1 + gamma_2 * gamma_2 + gamma_3*gamma_3;

        return A * std::exp(-num / (2.0*sigma*sigma)); 
    

    }

    inline Real exact_heat_4D(const Vector<Real, 4> &x){ 

        // return 100.0*(x[0]*x[0] - x[0]) * (x[1]*x[1] - x[1]) * (x[2]*x[2] - x[2])  * (x[3]*x[3] - x[3]) * std::exp(-100.0 * ((x[0]-x[3]) * (x[0]-x[3]) + (x[1]-x[3]) * (x[1]-x[3]) + (x[2]-x[3]) * (x[2]-x[3])));
        // return std::sin(M_PI*x[0]) * std::sin(M_PI*x[1]) * std::sin(M_PI*x[2]) * (1.0-x[3]) * (1.0-x[3]);
        // return (x[0]* x[0] - x[0]) * (x[1]* x[1] - x[1]) * (x[2]* x[2] - x[2]) * (x[3]* x[3] - x[3]) * std::exp(-100.0 * ((x[0] - 0.25) * (x[0] - 0.25) + (x[1] - 0.25) * (x[1] - 0.25) + (x[2] - 0.25) * (x[2] - 0.25) + (x[3] - 0.25) * (x[3] - 0.25)));
        // return x[0]*x[1]*x[2]*x[3]*(1.0-x[0])*(1.0-x[1])*(1.0-x[2])*(1.0-x[3]);  // Poisson test 

        // return (1-x[0]) * x[0] * x[0] * (1-x[1]) * x[1] * x[1] * (1-x[2]) * x[2] * x[2] * (1-x[3]) * x[3] * x[3]; 

                     
        Real A = 1; 
        Real sigma = 5e-02;
        Real r = 0.5; 
        Real p = 5;

        Real gamma_1 = x[0] - r * (std::cos(2*M_PI *x[3] / p));
        Real gamma_2 = x[1] - r * (std::sin(2*M_PI *x[3] / p));
        Real gamma_3 = x[2] - r * (std::cos(2*M_PI *x[3] / p));
        
        Real num = gamma_1 * gamma_1 + gamma_2 * gamma_2 + gamma_3*gamma_3;

        return A * std::exp(-num / (2.0*sigma*sigma));

    }   

}


void test_space_time_4D(int &n_lev){
    using namespace mars;
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;

    std::string path_matrix_stiff = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/matrix_csv/stiff.csv";
    std::string path_matrix_mass = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/matrix_csv/mass.csv";
    std::string path_rhs = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/matrix_csv/rhs.csv";
    std::string path_sol = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/matrix_csv/my_sol.csv";
    std::string path_exact = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/matrix_csv/exact.csv";

    
    CSVWriter csv;

    Mesh4 mesh;
    
    // read("/Users/liudmilakaragyaur/code/space_time_error_estimator/external/mars/data/cube4d_24.MFEM", mesh, true);
    // mesh.renumber_nodes();

    read("/Users/liudmilakaragyaur/code/test_pentatope.MFEM", mesh, true);
    mesh.renumber_nodes();
    // mark_boundary(mesh);   

	Quality<Mesh4> q(mesh);
	q.compute();
	mark_boundary(mesh);  
    mesh.build_dual_graph(); 
    print_boundary_info(mesh, true);


    Bisection<Mesh4> b(mesh);
	b.set_edge_select(std::make_shared<UniqueLongestEdgeSelect<Mesh4>>());
	b.uniform_refine(n_lev);
    print_boundary_info(mesh, true);

    mesh.clean_up();
    mesh.update_dual_graph();
    std::cout << "N_nodes  -> " << mesh.n_nodes() << std::endl;
    std::cout<< "N_elements -> " <<  mesh.n_elements() << std::endl;

    // mesh.describe(std::cout);

    Integer n_nodes = mesh.n_nodes();

    // for (int k = 0; k<n_nodes; ++k){
    //     mesh.point(k)[3] *= 0.1;
    // }


    bool space_time = true;
    MassMatrixAssembler<4,4> M;
    StiffnessMatrixAssembler<4,4> S;

    SparseMatrix mass_no_upwind = assemble_mass_stiffness(mesh, M, S, space_time, true, false);
    SparseMatrix mass_upwind = assemble_mass_stiffness(mesh, M, S, space_time, true, true);
    SparseMatrix stiffness = assemble_mass_stiffness(mesh, M, S, space_time, false, true);
    // csv.write_sparse(path_matrix_stiff, n_nodes, stiffness);



    // display_sparse(mesh,mass_upwind);
    std::vector<bool> node_is_boundary(mesh.n_nodes(), false);
    boundary_nodes(mesh, node_is_boundary, space_time); 

    const size_t N = mesh.n_nodes();
    Kokkos::View<Real*> st_rhs("st_rhs", N );
    Kokkos::View<Real*> result_st("result_st", N );
    Kokkos::View<Real*> exact_st("exact_st", N );
    Kokkos::View<Real*> init_guess("init_guess", N );
    fill_vector(init_guess, N, 0.5);

   
    for (int k = 0; k<N; ++k){
        st_rhs(k) = rhs_heat_4D(mesh.point(k));
        //
        exact_st(k) = exact_heat_4D(mesh.point(k));
    }   

    // csv.write_sparse_vector(" /Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/exact_sol.csv", N. exact_st);
    
    Kokkos::View<Real*> Mb("Mb", N );

    KokkosSparse::spmv("N", 1.0, mass_upwind, st_rhs, 0.0, Mb);

    Integer side_tag = -1;
    assign_boundary_cond(mesh, stiffness, Mb, node_is_boundary); 
    assign_init_cond(mesh, init_heat_4D, Mb);
    // display_vector(Mb,N);

    csv.write_sparse("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/matrix/uniform_matrices/matrix" + std::to_string(n_lev) + ".csv", n_nodes, stiffness);

    // csv.write_sparse(path_matrix_mass, n_nodes, mass_upwind);
    // csv.write_sparse(path_matrix_stiff, n_nodes, stiffness);
    // csv.write_sparse_vector(path_rhs,N,Mb);


    // // display_vector(Mb,N);
    Real num_iter;
    preconCG(stiffness, mass_upwind, Mb, node_is_boundary, 0, init_guess, result_st, num_iter); 

    // display_vector(exact_st, N);
    // std::cout << "===================" << std::endl;
    // display_vector(result_st, N);

    
    csv.write_sparse_vector(path_sol,N,result_st);
    csv.write_sparse_vector(path_exact,N,exact_st);


    // csv.write_sparse_vector(path_rhs,N,result_st);

   
    std::vector<Integer> elements_to_refine;
    Real TOL  = 1e-2;
    KokkosVector E("E", N );
    Real mean_error_RG;
    std::string path_error = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/error.vtu";
    rec_grad(mesh, mass_no_upwind, result_st,  node_is_boundary, TOL, elements_to_refine, mean_error_RG, path_error);
    Real TOL1 = 1e-4;
    Real TOL2 = 1e-4;
    Integer max_it = 100;

    

    Real mean_error_RB;
    std::string path_res_bas = "/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/error_res_based.vtu";
    std::vector<Integer> elements_to_refine_RB;
    res_based(mesh, result_st, rhs_heat_4D, elements_to_refine_RB, mean_error_RB, -1,-1, TOL,path_res_bas);

    // adaptive_scheme( mesh, TOL1, TOL2, max_it, result_st, elements_to_refine, side_tag, rhs_heat_4D, init_heat_4D, exact_heat_4D, true, true);
    
    // adaptive_scheme(mesh, TOL1, TOL2, max_it, result_st, elements_to_refine_RB, side_tag, rhs_heat_4D, init_heat_4D,exact_heat_4D, true, false);

    
    Real tot_error = 0;
    compute_exact_error(mesh, exact_st, result_st,tot_error);
    std::cout<< "True error: " << tot_error << std::endl;

    FILE* file = fopen("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_4D/uniform_ref/output.csv", "a");
    fprintf(file, "%ld, %e, %e, %e\n",mesh.n_nodes(), mean_error_RB, mean_error_RG,tot_error);
    fclose (file);





}





int main(int argc, char *argv[])
{
	using namespace mars;

    Kokkos::initialize(argc,argv);

    char *end_ptr = argv[1];
    int level = strtol(argv[1], &end_ptr, 10);

    test_space_time_4D(level);

    Kokkos::finalize();

    return 0; 
}