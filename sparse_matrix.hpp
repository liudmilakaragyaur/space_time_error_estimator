#ifndef ST_SPARSE_HPP
#define ST_SPARSE_HPP

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <map>

#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_utils.hpp"
#include "mars_simplex.hpp"
#include "mars_lagrange_element.hpp"
#include "mars_mesh.hpp"
#include "mars_bisection.hpp"
#include "mars_vtk_writer.hpp"
#include "mars_quality.hpp"
#include "mars_utils.hpp"
#include "mars_mesh_partition.hpp"
#include "mars_partitioned_bisection.hpp"
#include "mars_benchmark.hpp"
#include "mars_lepp_benchmark.hpp"
#include "mars_test.hpp"
#include "mars_ranked_edge.hpp"
#include "mars_oldest_edge.hpp"
#include "mars_longest_edge.hpp"
#include "mars_memory.hpp"
#include "mars_mesh_reader.hpp"
#include "mars_mesh_writer.hpp"
#include "mars_matrix.hpp"
#include "st_utils.hpp"
#include "st_quadrature.hpp"

// #include "diego_assembly.h" 

#include <err.h>
#include <Kokkos_Core.hpp>
#include "KokkosSparse_CrsMatrix.hpp"
#include <KokkosSparse_spmv.hpp>
#include<KokkosSparse_spadd.hpp>


namespace mars{
    using SparseMatrix = KokkosSparse::CrsMatrix<Real, Integer, Kokkos::Serial>;

    template<Integer Dim, Integer ManifoldDim>
    inline SparseMatrix assemble( std::vector<Real> values,  std::vector<Integer> rows,  std::vector<Integer> columns,const Mesh<Dim,ManifoldDim> &mesh) {
        // Create pair row-column 
        std::map<std::pair<Integer, Integer>, Real> mass_map;  ;
        std::pair<Integer, Integer> row_col;  

        for (int i = 0; i < rows.size(); i++) {
            row_col = std::make_pair(rows[i], columns[i]);
            mass_map[row_col] +=  values[i]; 
            
        }

        std::vector<Integer> row_ptr(mesh.n_nodes()+1, 0); 
        rows.clear();
        columns.clear();
        values.clear();

        // std::cout << " \nMAP \n";

        for (auto i: mass_map) {
        // std::cout << i.first.first << '|' << i.first.second << '|' << i.second << std::endl;
        values.push_back(i.second); 
        columns.push_back(i.first.second);
        row_ptr[i.first.first+1]++;
        }

        for (int i = 1; i < mesh.n_nodes()+1; i++){
            row_ptr[i] = row_ptr[i] + row_ptr[i-1];
        }


        // std::vector allows to extract the pointer of the wrapped array by doing &vector[0] --> points to the first element of the std::vector
        SparseMatrix sparse_matrix("A", mesh.n_nodes(), mesh.n_nodes(), values.size(), &values[0], &row_ptr[0], &columns[0]);  



        return sparse_matrix;
    }  

    template<Integer Dim, Integer ManifoldDim>
    void display_sparse(const Mesh<Dim,ManifoldDim> &mesh, const SparseMatrix &mat){

        for(Integer i = 0; i < mesh.n_nodes(); ++i) {
        auto row = mat.row(i);
        auto n_vals = row.length; // is the number of entries in the row 
//        assert(n_vals != 0);
            for(Integer k = 0; k < n_vals; ++k) {
                auto value = row.value(k);
                auto j = row.colidx(k); // returns a const reference to the column index of the k-th entry in the row </li>
                std::cout << i << ", " << j << " -> " << value << std::endl;
            }
        }

    }

    template<Integer Dim, Integer ManifoldDim>
    inline SparseMatrix assemble_mass_stiffness(
    Mesh<Dim,ManifoldDim> &mesh, 
    MassMatrixAssembler<Dim,ManifoldDim> &M,
    StiffnessMatrixAssembler<Dim,ManifoldDim> &S,
    const  bool &space_time = false, 
    const bool &mass_mat = false,
    const bool &upwind = false){

        Quadrature<0,Dim> q0; 
        Quadrature<1,Dim> q1; //change to 1 for 2D

    
        std::vector<Vector<Real, Dim>> qps, qp;
        std::vector<Real> qws, qw;

        // write("/Users/liudmilakaragyaur/code/space_time_error_estimator/mesh.msh", mesh);


        qps = q0.get_points();
        qws = q0.get_weights(); 

        qp = q1.get_points();
        qw = q1.get_weights();


        std::vector<Real> values; 
        std::vector<Integer> rows, columns; 
        Real max_d;
        Real min_d;
        max_diam(mesh,max_d);
        min_diam(mesh,min_d);

        FILE* file = fopen("/Users/liudmilakaragyaur/code/space_time_error_estimator/test_3D/matrix/diam.csv", "a");
        fprintf(file, "%ld, %e, %e\n",mesh.n_nodes(), max_d, min_d);
        fclose (file);  

        
        //max_d - 0.001;

        for (int k = 0; k < mesh.n_elements(); k++) {
            FE<Dim,ManifoldDim> element;

            const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(k);
            std::array<Integer, ManifoldDim+1> &nodes = mesh.elem(k).nodes; 


            if (Dim == 2){
                if (volume(simplex, mesh.points()) < 0){
                    element.fix_volume(nodes); 
                }
            }

            // std::cout << volume(simplex, mesh.points()) <<  std::endl;
        
            element.elem_diam(mesh,k);
            element.init_jacobian(mesh, k); 
            element.points(mesh,k);

            Real theta; 
            // theta =  element.diam; //-0.001;
            theta = 0.4;
            // theta = 1.0e-2;
            // theta = 1.0e-3;
            // theta = 1.0e-8;
            // std::cout << "THETA -> "<< theta << std::endl;

            // std::cout<< "Volume: " << 1.0/24.0 * element.det_J << " - " << element.measure  <<  std::endl; 

            Matrix<Real,ManifoldDim+1, ManifoldDim+1> local;

            if (mass_mat) {
                M.assemble(element,qp, qw, local,  element.diam,  theta, space_time, upwind); //element.diam, theta,
                // local.describe(std::cout);
            } else {
                S.assemble(element,qps, qws, local,  element.diam,  theta, space_time, upwind); //element.diam, theta,

                // local.describe(std::cout);
            }



            for (int i = 0; i < ManifoldDim+1; ++i) {
                for (int j = 0; j < ManifoldDim+1; ++j) {
                    if (local(i,j) != 0) {
                        values.push_back(local(i,j)); 
                        rows.push_back(nodes[i]); 
                        columns.push_back(nodes[j]);
                    }
                }
            }


        }

        mesh.update_dual_graph();
        mark_boundary(mesh);   
        
        SparseMatrix matrix = assemble(values,rows,columns, mesh);
        return matrix;

    }


    void display_vector(const KokkosVector &vector, const size_t N){
        for(int i=0;i<N;i++){
            std::cout<<vector(i)<<std::endl;
        }

    }

    void fill_vector(const KokkosVector &vector, const size_t N, Real value){
    for(int i=0;i<N;i++){
        vector(i) = value;
    }

    }

    void invert_diag_vec (KokkosVector &vec, KokkosVector &diag, const size_t N){
        for(Integer i = 0; i < N; ++i) {
            diag(i) = 1./vec(i);
        }
    }

    void vec_to_kokkos( const std::vector<Real> &vec ,KokkosVector &kokkos){
        for(Integer i = 0; i < vec.size(); ++i) {
            kokkos(i) = vec[i];
        }
    }

    void invert_diag_sparse (SparseMatrix &mat, KokkosVector &diag){
        Integer N =  mat.numRows();
        for(Integer i = 0; i < N; ++i) {
            auto row = mat.row(i);
            auto n_vals = row.length;
            for(Integer k = 0; k < n_vals; ++k) {
                auto j = row.colidx(k);
                if (i==j){
                    diag(i) = 1./row.value(k);
                }
            }
        }
    }

    void lumped_mat (SparseMatrix &mat, KokkosVector &diag){
    Integer N =  mat.numRows();
    for(Integer i = 0; i < N; ++i) {
        auto row = mat.row(i);
        auto n_vals = row.length;
        Real tot_row = 0.0;
        for(Integer k = 0; k < n_vals; ++k) {
            // auto j = row.colidx(k);
            tot_row += row.value(k);
        }

        diag(i) = tot_row;

    }
}

    



}

#endif //ST_SPARSE_HPP